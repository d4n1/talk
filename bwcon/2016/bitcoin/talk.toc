\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Me}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Me}{4}{0}{1}
\beamer@sectionintoc {2}{Criptocurrency}{14}{0}{2}
\beamer@subsectionintoc {2}{1}{Cryptocurrency}{14}{0}{2}
\beamer@subsectionintoc {2}{2}{Features}{16}{0}{2}
\beamer@subsectionintoc {2}{3}{History}{21}{0}{2}
\beamer@subsectionintoc {2}{4}{Legality}{26}{0}{2}
\beamer@sectionintoc {3}{Bitcoin}{32}{0}{3}
\beamer@subsectionintoc {3}{1}{Bitcoin}{32}{0}{3}
\beamer@subsectionintoc {3}{2}{Bitcoin Developers}{34}{0}{3}
\beamer@subsectionintoc {3}{3}{Bitcoin Facts}{36}{0}{3}
\beamer@subsectionintoc {3}{4}{Bitcoin for All}{49}{0}{3}
\beamer@subsectionintoc {3}{5}{Amount of Bitcoin Clients}{64}{0}{3}
\beamer@subsectionintoc {3}{6}{Amount of Bitcoin Transactions}{66}{0}{3}
\beamer@subsectionintoc {3}{7}{Bitcoin System}{68}{0}{3}
\beamer@sectionintoc {4}{Bitcoin Elements}{70}{0}{4}
\beamer@subsectionintoc {4}{1}{Wallet}{70}{0}{4}
\beamer@subsectionintoc {4}{2}{Miner}{79}{0}{4}
\beamer@subsectionintoc {4}{3}{Blockchain}{87}{0}{4}
\beamer@sectionintoc {5}{Cracking}{89}{0}{5}
\beamer@subsectionintoc {5}{1}{Cracking}{89}{0}{5}
\beamer@sectionintoc {6}{Hacking}{94}{0}{6}
\beamer@subsectionintoc {6}{1}{Hacking}{94}{0}{6}
\beamer@sectionintoc {7}{References}{102}{0}{7}
\beamer@subsectionintoc {7}{1}{References}{102}{0}{7}
\beamer@sectionintoc {8}{End}{106}{0}{8}
\beamer@subsectionintoc {8}{1}{End}{106}{0}{8}
