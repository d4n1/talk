def FizzBuzz():
    for i in range(1,101):
        print {
            3 : "Fizz",
            5 : "Buzz",
            15 : "FizzBuzz"}.get(15*(not i%15) or
                                 5*(not i%5 ) or
                                 3*(not i%3 ), '{}'.format(i))
