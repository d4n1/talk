\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Eu}{4}{0}{1}
\beamer@sectionintoc {2}{A For\IeC {\c c}a}{14}{0}{2}
\beamer@subsectionintoc {2}{1}{A For\IeC {\c c}a}{14}{0}{2}
\beamer@subsectionintoc {2}{2}{Origem da For\IeC {\c c}a}{16}{0}{2}
\beamer@sectionintoc {3}{Padawan e Jedi}{23}{0}{3}
\beamer@subsectionintoc {3}{1}{Padawans e Jedis}{23}{0}{3}
\beamer@subsectionintoc {3}{2}{Caracter\IeC {\'\i }sticas de um Padawan}{25}{0}{3}
\beamer@subsectionintoc {3}{3}{Caracter\IeC {\'\i }sticas de um Jedi}{36}{0}{3}
\beamer@sectionintoc {4}{C\IeC {\'o}digo Jedi}{47}{0}{4}
\beamer@sectionintoc {5}{Treinamento Jedi}{53}{0}{5}
\beamer@subsectionintoc {5}{1}{Forjando os cristais llum}{53}{0}{5}
\beamer@subsectionintoc {5}{2}{Treinamento de Sabre de Luz I}{57}{0}{5}
\beamer@subsectionintoc {5}{3}{Treinamento de Sabre de Luz II}{67}{0}{5}
\beamer@sectionintoc {6}{Lado Negro da For\IeC {\c c}a}{71}{0}{6}
\beamer@sectionintoc {7}{Teste Jedi}{74}{0}{7}
\beamer@sectionintoc {8}{Desafios Jedi}{76}{0}{8}
\beamer@sectionintoc {9}{Refer\IeC {\^e}ncias}{78}{0}{9}
\beamer@sectionintoc {10}{Fim}{79}{0}{10}
