\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Eu}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Eu}{4}{0}{1}
\beamer@sectionintoc {2}{Criptomoedas}{13}{0}{2}
\beamer@subsectionintoc {2}{1}{Cryptomoedas?}{13}{0}{2}
\beamer@subsectionintoc {2}{2}{Caracter\IeC {\'\i }sticas}{15}{0}{2}
\beamer@subsectionintoc {2}{3}{Hist\IeC {\'o}ria}{21}{0}{2}
\beamer@subsectionintoc {2}{4}{Legalidade}{26}{0}{2}
\beamer@sectionintoc {3}{Bitcoin}{32}{0}{3}
\beamer@subsectionintoc {3}{1}{Bitcoin?}{32}{0}{3}
\beamer@subsectionintoc {3}{2}{Caracteristicas}{34}{0}{3}
\beamer@subsectionintoc {3}{3}{Bitcoin para Todos}{42}{0}{3}
\beamer@subsectionintoc {3}{4}{Quantidade de Clientes}{56}{0}{3}
\beamer@subsectionintoc {3}{5}{Quantidade de Transa\IeC {\c c}\IeC {\~o}es}{58}{0}{3}
\beamer@subsectionintoc {3}{6}{Sistema}{60}{0}{3}
\beamer@sectionintoc {4}{Elementos Bitcoin}{62}{0}{4}
\beamer@subsectionintoc {4}{1}{Wallet}{62}{0}{4}
\beamer@subsectionintoc {4}{2}{Miner}{71}{0}{4}
\beamer@subsectionintoc {4}{3}{Block chain}{80}{0}{4}
\beamer@sectionintoc {5}{Cracking}{82}{0}{5}
\beamer@subsectionintoc {5}{1}{Cracking}{82}{0}{5}
\beamer@sectionintoc {6}{Hacking}{87}{0}{6}
\beamer@subsectionintoc {6}{1}{Hacking}{87}{0}{6}
\beamer@sectionintoc {7}{Refer\IeC {\^e}ncias}{96}{0}{7}
\beamer@subsectionintoc {7}{1}{Refer\IeC {\^e}ncias}{96}{0}{7}
\beamer@sectionintoc {8}{Fim}{99}{0}{8}
\beamer@subsectionintoc {8}{1}{Fim}{99}{0}{8}
