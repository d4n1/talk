# Vim

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Vim

- Editor de texto
- Editor de código
- IDE

---

# Install

    !zsh
    sudo apt install vim

---

# Vim Script

    !vim
    :let name = 'd4n1'
    :echo "Hello, " . name

---


# Variables

    !vim
    let name = "d4n1"
    let g:ack_options = '-s -H'    " g: global
    let s:ack_program = 'ack'      " s: local (to script)
    let l:foo = 'bar'              " l: local (to function)
    let w:foo = 'bar'    " w: window
    let b:state = 'on'   " b: buffer
    let t:state = 'off'  " t: tab
    echo v:var           " v: vim special
    let @/ = ''          " @  register (this clears last search pattern)
    echo $PATH           " $  env
    a + b             " numbers only!
    'hello ' . name   " concat
    let var -= 2
    let var += 5
    let var .= 'string'   " concat

---

# Strings

    !vim
    let str = "String"
    len(str)       " length
    strchars(str)  " character length
    split("one two three")       "=> ['one', 'two', 'three']
    join(['a', 'b'], ',')  "=> 'a,b'
    tolower('Hello')
    toupper('Hello')

---

# Functions

    !vim
    function! s:Initialize(cmd, args)
        " a: prefix for arguments
        echo "Command: " . a:cmd

        return true
    endfunction

    call s:Initialize()
    call s:Initialize("hello")
---

# Loops

    !vim

    for s in list
        echo s
        continue  " jump to start of loop
        break     " breaks out of a loop
    endfor

    while x < 5
    endwhile

---

# Flow

    !vim
    let char = getchar()
    if char == "\<LeftMouse>"
        " ...
    elseif char == "\<RightMouse>"
        " ...
    else
        " ...
    endif

    if empty(a:path) | return [] | endif

    if 1 | echo "true"  | endif
    if 0 | echo "false" | endif

    if name ==# 'd4n1'     " case-sensitive
    if name ==? 'D4n1'     " case-insensitive
    if name == 'D4n1'      " depends on :set ignorecase

    "hello" =~ 'xx*'
    "hello" !~ 'xx*'
    "hello" =~ '\v<\d+>'

---

# List

    !vim
    let list = [1, two, 3, "four"]

    let shortlist = list[2:-1]

    empty(list)
    sort(list)
    
    let addlist = add(list, 4)

    call map(files, "bufname(v:val)")  " use v:val for value
    call filter(files, 'v:val != ""')

---

# Dict

    !vim
    let dict = {
        \ "apple": "red",
        \ "banana": "yellow"
    }

    remove(colors, "apple")

    if has_key(dict, 'foo')
    if empty(dict)
    keys(dict)
    max(dict)
    min(dict)
    count(dict, 'x')

---

# Casting

    !vim
    str2float("2.3")
    str2nr("3")
    float2nr("3.14")

---

# Numbers

    !vim
    let int = 1000
    let int = 0xff
    let int = 0755   " octal

    let fl = 100.1
    let fl = 5.4e4

    3 / 2     "=> 1, integer division
    3 / 2.0   "=> 1.5
    3 * 2.0   "=> 6.0

    sqrt(100)
    floor(3.5)
    ceil(3.3)
    abs(-3.4)

---

# Plugin

    !zsh
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

---

# vim ~/.vimrc

    !vim
    call plug#begin("~/.vim/plugged")
    Plug 'joshdick/onedark.vim'
    Plug 'itchyny/lightline.vim'
    Plug 'sheerun/vim-polyglot'
    Plug 'mattn/emmet-vim'
    Plug 'posva/vim-vue'
    call plug#end()
    
    syntax on
    colorscheme onedark
    let g:lightline = { 'colorscheme': 'onedark' }
    set encoding=utf8
    set textwidth=119
    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set autoindent
    set expandtab
    set smarttab
    set laststatus=2
    set ignorecase
    set t_Co=256
    set noshowmode
    set cursorline
    set hlsearch

---

# Shortcuts

    !vim
    :q        " close
    :w        " write/saves
    :wa[!]    " write/save all windows [force]
    :wq       " write/save and close
    :x        " save and quit, same as wq
    :q!       " force close if file has changed and not save changes

---

# Easter Eggs

    !vim
    :help 42
    :smile
    :help!
    :h holy-grail
    :h map-modes
    :h UserGettingBored
    :h spoon
    :h showmatch
    :h bar
    :Ni!

    :! telnet towel.blinkenlights.nl
---

# Referências

- [Vim](https://www.vim.org/docs.php)

---

# d4n1.org

![](assets/avatar.png)
