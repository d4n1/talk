% GNU/Linux
% Daniel Pimentel
% d4n1.org

# SystemD

- Units
- Cgroups
- Targets
- Sockets

# Informações

- systemctl status
- systemctl --failed
- systemctl list-unit-files

# Gerenciamento

- systemctl
- systemctl start | stop | restart
- systemctl status
- systemctl enable | disable

# UCK

- Linguagem
- Desktop
- Nome
- Imagem híbrida

# GUI

- Run package manager
- Run console application
- Continue building

# Run package manager

- Synapctics
- GNOME Packages

# Run console application

- apt install tasksel | tasksel
- tasksel --list-tasks | --task-desc
- tasksel --task-packages

# Continue building

- Finaliza a ISO

# Copiando

- dd if=image.iso of=/dev/sdb bs=1M

# Clonando

- Clonezilla live
- Escolha a linguagem
- Selecione o teclado
- Iniciar Clonezilla

# Clonando

- Escolha device-image
- Escolha local_dev
- Escolha o HD
- Escolha o diretório da imagem

# Clonando

- Escolha o modo iniciante
- Escolha savedisk
- Escolha o nome da imagem
- Selecione o disco a ser clonado

# Clonando

- Pule a checagem de sistema
- Selecione a checagem da imagem
- Escolha o nome da imagem
- Selecione não criptografar os dados

# Clonando

- Saia do Clonezilla

# Restaurando

- Clonezilla live
- Escolha a linguagem
- Selecione o teclado
- Iniciar Clonezilla

# Restaurando

- Escolha device-image
- Escolha local_dev
- Escolha o HD
- Escolha o diretório da imagem

# Restaurando

- Escolha o modo iniciante
- Escolha restoredisk
- Selecione a imagem
- Selecione o disco alvo

# Restaurando

- Confirme a restauração
- Saia do Clonezilla

# Obrigado
