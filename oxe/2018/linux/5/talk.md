% GNU/Linux
% Daniel Pimentel
% d4n1.org

# Gerenciadores

- dpkg
- apt
- synaptic

# DPKG

- dpkg -i [pacote]
- dpkg -l [pacote]
- dpkg -r | -P
- dpkg -I | -S

# Repositórios

- /etc/apt/sources.list
- /etc/apt/apt.conf

---
![](assets/release-cycle.png)

# APT

- apt search
- apt install | remove | purge
- apt update | upgrade | dist-upgrade
- apt clean | autoremove

# Manual

- tar -vxfjz [pacote] && cd [pacote]
- ./configure
- make
- make install

# Obrigado
