% Debian Packager
% Daniel Pimentel
% Oxe Hacker Club

# NMU

```
dch --nmu
dput -e <day> <changes>
nmudiff --no-mutt
dcut -k <key> cancel <changes> 
```

# GBP

```
gbp export-origin

git clone <repo>
cd <upstream>
git checkout upstream
git checkout debian/master

gbp import-orig ../<upstream>
gbp dch
apt build-deps <upstream>
debuild
gbp tag
gbp import-dsc <dsc>

gbp import-dscs --debsnap <package>
```

# GBP

```
vim ~/.gbp.conf

[DEFAULT]
git-debian--branch= debian/master

[GBP]
pristine-tar=True
git-log=--no-merges
tar <new>
gbp import-orig ../<new>
gbp dch
git commit -m '<msg>'
debuild
git push --all --tag
```

# Exercício

>- NMU

# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Obrigado [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
