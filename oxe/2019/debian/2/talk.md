% Debian Packager
% Daniel Pimentel
% Oxe Hacker Club

# Search

```
apt search <upstream>
wnpp-check <upstream>
```

# Upstream

```
wget <upstreal-url>
tar -xvf upstream-1.0.tar.xz
cd upstream-1.0
```

# License

```
licensecheck --deb-machine --merge-licenses -r *
egrep -sriA25 '(copyright|public dom)'
egrep '(@|\[)' <changelog>
```

# Make

```
dh_make -f ../upstream-1.0.tar.xz -c <license>
```

# Packaging

```
cd debian
mv compat <upstream>.* manpage.* menu.* post* pre* \
    README.* watch* *.docs /tmp
vim changelog copyright control rules source/format \
    tests/control watch
```

# Build

```
cd ..
debuild
```

# Check

```
lintian -i
```

# Scan

```
uscan
```

# Hardening

```
blhc --all --debian <upstream.build>
```

# Install

```
debi
```

# CI

```
autopkgtest . -- null
```

# Test

```
apt install cowbuilder
cowbuilder create
cowbuilder build <upstream.dsc>
```

# Check

```
debdiff <upstream-1.dsc> <upstream-2.dsc>
```

# Send

```
dput mentors <upstream.changes>
```

# Exercício

>- Empacotar


# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Obrigado [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
