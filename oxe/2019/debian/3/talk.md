% Debian Packager
% Daniel Pimentel
% Oxe Hacker Club

# Update

```
uupdate ../<upstream-tarbal>
```

# Changelog

```
dch -i
debuild
```

# Patch

```
vim <file>
dpkg-source --commit
quilt push -a
quilt pop -a
```

# QA

```
apt source <package>
dpkg-source -x <package.dsc>
dch --qa
vim debian/control
apt build-dep <package>
```

# Exercício

>- QA

# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Obrigado [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
