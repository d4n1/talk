% Debian Packager
% Daniel Pimentel
% Oxe Hacker Club

# Eu

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Maker
>- Hackativista
>- Músico
>- Motociclista
>- Skatista
>- Geek

# Pacotes

>- +59k

# Feramentas

>- dpkg
>- apt
>- apt-get/apt-cache
>- aptitude
>- synaptic

# Pessoas

>- Packager
>- Debian Maintainer
>- Debian Developer
>- Sponsor
>- Leader

# Desenvolvedores

![DDs arround the world](assets/developers.jpg){width=93%}

# Conceitos

>- Upstream
>- Source
>- Binary

# GPG

```
gpg --full-gen-key
```

# Docker

```
vim dockerfile
docker build -t debian .
docker run -it -v `pwd`/packages:/packages debian
```

# Email

>- debian-announce
>- debian-devel-announce
>- debian-infrastructure-announce
>- debian-legal
>- debian-mentors
>- debian-br-eventos

# Site

>- [Tracker](https://tracker.debian.org)
>- [Bug](https://bugs.debian.org)
>- [Packages](https://packages.debian.org)
>- [Lintian](https://lintian.debian.org)
>- [Mentors](https://mentors.debian.net)
>- [Snapshot](https://snapshot.debian.org)

# Ajuda

>- Documentação
>- Tradução
>- Empacotamento
>- Artwork
>- Mirrors
>- Doação 
>- Bug :)

# Exercício

>- Docker

# Referências

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Obrigado [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
