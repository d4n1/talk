---
title: Python
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Resposta do exercício anterior

```
# Crie uma matriz 3x3:
matrix = [[0, 1, 2], [3, 4, 5], [6, 7, 8]]

# Multiplique os valores dos elementos por 4
plus = [[x * 4 for y in matrix[:1][:] for x in y], \
        [x * 4 for y in matrix[1:2][:] for x in y], \
        [x * 4 for y in matrix[2:3][:] for x in y]]

# Transforme a matriz em matriz identidade
identity = [[0] * i + [1] + [0] * (len(matrix) - i - 1)\
           for i in range(len(matrix))]
```

# Operador módulo

```
quociente = 7 // 3
print(quociente)

resto = 7 % 3
print(resto)
```

# Expressões boleanas

```
5 == 5
5 == 6
x = 4
y =  42
x != y
x > y
x < y
x >= y
x <= y
```

# Operadores lógicos

```
x = 8
x > 0 and x < 10

n = 13
n % 2 == 0 or n % 3 == 0
```

# Execução condicional

```
x = 42
if x > 0
  print("x é positivo")
```

# Execução alternativa

```
x = 8
if x % 2 == 0:
  print(x, "é par")
else:
  print(x, "é impar")
```

# Condicionais encadeados

```
x = 42
y = 8
if x < y:
  print(x, "é menor que", y)
elif x > y:
  print(x, "é maior que", y)
else:
  print(x, "e", y, "são iguais")
```

# Condicionais aninhados

```
x = 8
y = 42
if x == y:
  print(x, "e", y, "são iguais")
else:
  if x < y:
    print(x, "é menor que", y)
  else:
    print(x, "é maior que", y)

if 0 < x < 10:
  print("x é um número positivo de um só algarismo.")
```

# Mutabilidade e tuplas

>- Strings são imutáveis
>- Listas são mutáveis
>- Tuplas são "listas" imutáveis

# Mutabilidade e tuplas

```
tupla = ’a’, ’b’, ’c’, ’d’, ’e’
tupla = (’a’, ’b’, ’c’, ’d’, ’e’)

t1 = (’a’,)
type(t1)

t2 = (’a’)
type(t2)

tupla = (’a’, ’b’, ’c’, ’d’, ’e’)
tupla[0]
tupla[1:3]
tupla[0] = ’A’
tupla = (’A’,) + tupla[1:]
tupla
```

# Atribuições de tupla

```
a, b = 4, 8
```

# Dicionários

```
dicionario = {}
dicionario[’one’] = ’um’
dicionario[’two’] = ’dois’
print(dicionario)
```

# Operações dos dicionários

```
inventario = {’abacaxis’: 430, ’bananas’: 312, \
              ’laranjas’: 525, ’peras’: 217}
print(inventario)

del inventario[’peras’]
print(inventario)

inventario[’peras’] = 0
print(inventario)

len(inventario)
```

# Métodos dos dicionários

```
inventario = {’linguagem’: 'Python', ’versão’: '3.6.4'}

inventario.keys()
inventario.values()
inventario.items()

inventario.get(’linguagem’)

'3.6.4' in inventario
```

# Aliasing e copy

```
opposites = {’up’: ’down’, ’right’: ’wrong’, \
             ’true’: ’false’}

alias = opposities
copy = opposities.copy()

alias[’right’] = ’left’
opossites[’right’]

copy[’right’] = ’privilege’
opposites[’right’]
```

# Exercício


>- Faça um programa que receba dois números e mostre o maior
>- Faça  um  programa que  receba  três  notas  de  um  aluno,  calcule  e  
    mostre  a  média  aritmética e imprima a situação do aluno 
    (0-3 reprovado, 3-7 exame e 7-10 aprovado). 
    Para alunos que ficaram para exame, calcule e mostre a nota que deverão 
    tirar para serem aprovados, considerando a média exigida é 6,0
>- Imprima todos os conjuntos de palavras que são anagramas
>- Descubra todos os pares de metátese

# Botfather

>- telegram.me/botfather
>- /newbot
>- [bot]\_bot

# Broser

>- api.telegram.org/bot[token]/getme
>- api.telegram.org/bot[token]/getUpdates

# Bot

>- /start

# Code

>- Show me the code :)

# Desafio

>- Desenvolver novas features

# Obrigado [^1]

![d4n1.org](assets/avatar.jpg){width='44%'}

[^1]: https://d4n1.org
