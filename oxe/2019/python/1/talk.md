---
title: Python
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Maker
>- Hacktivista
>- Músico
>- Motociclista
>- Skatista
>- Geek

# Introdução

>- Criado em 1990 por Guido Van Rossum
>- Código aberto
>- Linguagem de programação de alto nível
>- Linguagem interpretada

# Características

>- Multiparadigma
>- Tipagem dinâmica e forte
>- Interpretada via bytecode
>- Sintaxe clara e concisa
>- Multiplataforma
>- Multipropósito
>- Baterias inclusas

# Depuração

>- Bug
>- Debug
>- Erro de sintaxe
>- Exceções
>- Erro de semântica

# Filosofia

```
import this
```

# Easter Eggs

```
import __hello__
from __future__ import braces
print('We are the {} who say "{}!"'.format( \
    'knights', 'Ni'))
import antigravity
import this; print(this.i, this.d, this.c, this.s)
```

# Primeiro programa

```
print("Python Padawan")
```

# Valores e tipos

```
type(42)
type("Padawan")
type(3.14)
```

# Variáveis

```
var = 42
type(var)
var = "Padawan"
type(var)
var = 3.14
type(var)
```

# Variáveis e palavras reservadas

```
42padawan = "Padawan"
padawan$ = 42
class = "Padawan"

import keyword
print(keyword.kwlist)
```

# Comandos

```
print(4)
x = 42
print(8)
```

# Avaliando expressões

```
41 + 1
mensagem = "Padawan"
mensagem
print(mensagem)
```

# Operadores e operandos

```
20 + 32
hora = 24
minuto = 42
hora - 1
hora * 60 + minuto
minuto / 60
minuto // 60
5 ** 2
(5 + 9) * (15 - 7)
```

# Ordem dos operadores

>- P (parentêses)
>- E (exponenciação)
>- MDAS (Multiplicação, Divisão, Adição e Subtração)
>- PEMDAS

# Ordem dos operadores

```
2 * (3-1)
2**1+1
2*3-1
```

# Operações com strings

```
padawan = "aprendiz"
força = " do lado da luz"
print(padawan + força)

"padawan" * 3
```

# Composição

```
print(4 + 4)

print("Minutos desde meia-noite: ", hora * 60 + minuto)
```

# Comentários

```
# Única linha

"""
Primeira linha
Segunda linha
...
"""

'''
Primeira linha
Segunda linha
...
'''
```

# Listas

>- Conjunto ordenado de Valores
>- Valores identificado por um índice
>- Elementos são os valores da lista

# Valores da lista

```
[4, 8, 13, 42]
[’python’, ’padawan’]
[’python’, 4.8, 4, [4, 8]]

list(range(4, 4))
list(range(8))
list(range(4, 8, 4))
```

# Acessando elementos

```
numeros = list(range(4))

print(numeros[0])

numeros[1] = 5
numeros[3-2]
numeros[1.0]
numeros[2] = 5
numeros[-1]
numeros[-2]
numeros[-3]
```

# Comprimento da lista

```
codigo = ['a emoção, ainda a paz',
    'a ignorância, ainda o conhecimento',
    'Paixão, ainda serenidade',
    'Caos, ainda a harmonia'
    'Morte, mas a Força]
len(codigo)
```

# Membros de uma lista

```
codigo = ['paz', 
    'conhecimento', 
    'serenidade', 
    'harmonia',
    'força']

'força' in codigo
'caos' in codigo
''morte'' not in codigo
```

# Operações em listas

```
a = [1, 2, 3]
b = [4, 5, 6]
c = a + b

print(c)

[0] * 4
[1, 2, 3] * 3
```

# Fatiamento em listas

```python
codigo[1:3]
codigo[:4]
codigo[3:]
codigo[:]
```

# Listas são mutáveis

```
codigo[4] = "morte"
codigo[-1] = "emoção"
print(codigo)

codigo[1:3] = ['ignorância', 
    'paixão',
    'caos']
print(codigo)
```

# Remoção em lista

```
del codigo[1]
print(codigo)

del codigo[1:2]
print(codigo)
```

# Objetos e valores

```
a = "python"
b = "padawan"
id(a)
id(b)

a = [1, 2, 3]
b = [1, 2, 3]
id(a)
id(b)
```

# Apelidos

```
a = [1, 2, 3]
b = a
print(a)
print(b)

b[0] = 5
print(a)
print(b)
```

# Clonando listas

```
a = [1, 2, 3]
b = a[:]
print(b)

b[0] = 5
print(a)
```

# Lista aninhadas

```
lista = ["alo", 
    2.0, 
    5, 
    [10, 20]]

elemento = lista[3]
elemento[0]

lista[3][1]
```

# Matrizes

```
matriz = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

matriz[1]

matriz[1][1]
```

# Exercício

>- Crie uma matriz 3x3
>- Multiplique os valores dos elementos por 4
>- Transforme a matriz em matriz identidade

# Desafio

>- Desenvolver um bot no telegram

# Obrigado [^1]

![d4n1](assets/avatar.jpg){width='44%'}

[^1]: http://d4n1.org
