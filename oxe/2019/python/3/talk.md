---
title: Python
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Chamadas de funções

```python
type(’32’)
bia = type(’32’)
print(bia)
id(3)
bia = 3
bia(beth)
```

# Conversão entre tipos

```python
int(’32’)
int(’Alô’)
int(3.99999)
int(-2.3)
float(32)
float(’3.14159’)
str(32)
str(3.14149)
```

# Coerção entre tipos

```python
minuto = 59
float(minuto) / 60
minuto = 59
minuto / 60.0
```

# Funções matemáticas

```python
import math
decibel = math.log10(17.0)
angulo = 1.5
altura = math.sin(angulo)
graus = 45
angulo = graus * 2 * math.pi / 360.0
math.sin(angulo)
math.sqrt(2) / 2.0
```

# Composição

```python
x = math.cos(angulo + pi/2)
x = math.exp(math.log(10.0))
```

# Adicionando novas funções

```python
def novaLinha() :
  print()
def tresLinhas() :
  novaLinha()
  novaLinha()
  novaLinha()
print(’Primeira Linha.’)
tresLinhas()
print (’Segunda Linha.’)
```

# Parâmetros e argumentos

```python
def imprimeDobrado(bruno):
  print(bruno, bruno)
imprimeDoobrado(’Spam’)
imprimeDobrado(3.14159)
```

# Variáveis e parâmetros são locais

```python
def concatDupla(parte1, parte2)
  concat = parte1 + parte2
imprimeDobrado(concat)
canto1 = ’Pie Jesu domine, ’
canto2 = ’dona eis requiem. ’
concatDupla(canto1, canto2)
print(concat)
```

# Valores de retorno

```python
import math
def area(raio):
  temp = math.pi * raio**2
  return temp
```

# Composição

```python
def area2(xc, yc, xp, yp):
  raio = distancia(xc, yc, xp, yp)
  resultado = area(raio)
  return resultado
```

# Funções booleanas

```python
def ehDivisivel(x, y):
  If x % y == 0:
    return True # é verdadeiro (True), é divisível
  else:
    return False # é falso (False), não é divisível
```

# Mais recursividade

```python
def fatorial(n):
  if n == 0:
    return 1
  else:
    recursivo = fatorial(n-1)
    resultado = n * recursivo
    return resultado
```

# O comando while

```python
def contagemRegressiva(n):
  while n > 0:
    print (n)
    n = n-1
  print("Fogo!")
```

# Tabelas

```python
x = 1.0
while x < 10.0:
  print(x, ’\t’, math.log(x))
  x = x + 1.0
```

# Tabelas de duas dimensões

```python
i = 1
while i <= 6:
  print(2*i)
  i = i + 1
print()
```

# O comando for

```python
lista = [1, 2, 3, 4]
for item in lista:
  print(lista)
```

# Else em loops

```python
for n in [10,9,8,7,6,5,4,3,2,1]:
  print(n)
else:
  print("python")
```

# Break, continue e pass

```python
for n in range(2, 10):
  for x in  range(2, n):
    if n % x == 0:
      print(n, ’equals ’, x, ’*’, n/x)
      break
  else:
    print(n, ’is a prime  number ’)
```

# Lambda

```python
def f(x):
  return x*2
f(3)
g = lambda x: x*2
(lambda x: x*2)(3)
```

# Decorators

```python
def our_decorator(func):
  def function_wrapper(x):
    print("Before calling " + func.__name__)
    func(x)
    print("After calling " + func.__name__)
  return function_wrapper

def foo(x):
    print("Hi, foo has been called with " + str(x))

print("We call foo before decoration:")
foo("Hi")
print("We now decorate foo with f:")
foo = our_decorator(foo)
print("We call foo after decoration:")
foo(42)
```

# Exceptions

```python
while True:
  try:
      n = input("Digite um inteiro: ")
      n = int(n)
      break
  except ValueError:
      print("Inteiro não válido! Tente novamente ...")
print(":)")
```

# Modules

```python
def fib(n):
  a, b = 0, 1
  while b < n:
    print(b, end=' ')
    a, b = b, a+b
  print()

def fib2(n):
  result = []
  a, b = 0, 1
  while b < n:
    result.append(b)
    a, b = b, a+b
  return result
import fibo as fib
from fibo import *
```

# Exercício

>- Escreva o cálculo de fibonaci usando lambda :)

# Obrigado [^1]

![d4n1.org](assets/avatar.jpg){width='44%'}

[^1]: https://d4n1.org
