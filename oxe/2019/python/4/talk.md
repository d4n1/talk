---
title: Python
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Classe

>- Abstração
>- Conjunto de objetos
>- Objetos ou instância

# Classe

```
class Padawan:
  pass

Padawan
dir(Padawan)
id(Padawan)
Padawan.__name__
anakin = Padawan
anakin
dir(anakin)
id(anakin)
anakin.__name__
```

# Atributos

```
class Padawan:
    name = 'Anakin'
    lastname = 'Skywalker'
```

# Atributos

```
anakin = Padawan
dir(anakin)
anakin.name
anakin.lastname
```

# Métodos{.allowframebreaks}

```
class Padawan:
    def __init__(self, name, lastname, level=0):
        self.name = name
        self.lastname = lastname
        self.level = level

    def train(self):
        self.level = self.level + 1
        print('Your level is {}!'.format(self.level))

    def force(self):
        if self.level <= 8:
            print('''
                Your level is {}.
                Train more, you will need!
                '''.format(self.level))
        elif self.level == 9:
            print('''
                Your level is {}.
                Is comming, the force!
                '''.format(self.level))
        else:
            print('''
                Your level is {}.
                You have, the force!

                There is no emotion, there is peace.
                There is no ignorance, there is knowledge.
                There is no passion, there is serenity.
                There is no chaos, there is harmony.
                There is no death, there is the Force.
                '''.format(self.level))
```

# Métodos

```
anakin = Padawan('Anakin', 'Skywalker')
anakin.name
anakin.lastname
anakin.level
anakin.train()
anakin.force()
```

# Docstring{.allowframebreaks}

```
class Padawan:
    """
    An Initiate who successfully completes fundamental 
    training is given a second-class education and then 
    undergoes Padawan training under the tutelage of a 
    Mentor (usually a Jedi Knight or Jedi Master).

    This class define a Padawan.
    """

    def train(self):
        """
        This method start a train to increase level.
        """
```

# Docstring

```
anakin.__doc__
anakin.train.__doc__
help(anakin)
```

# Herança

>- Reultilização
>- Generaliação
>- Especialização

# Herança{.allowframebreaks}

```
class Creature(object):
    """
    Class that define a creature.
    """

    def __init__(self, name, lastname, blood='Human', 
        level=0):
        """
        Method constructor.
        """

        self.name = name
        self.lastname = lastname
        self._blood = blood
        self.level = level


class Padawan(Creature):
    """
    Class that define a Padawan in Jedi order and 
    inherits some atributes of Creatures class.
    """

    def train(self):
        """
        Method that inscrease power level of a Padawan.
        """

        self.level = self.level + 1
        print('Your level is {}!'.format(self.level))

    def power(self):
        """
        Method that check power level.
        """

        if self.level <= 8:
            print('''
                Your level is {}.
                Train more, you will need!
                '''.format(self.level))
        elif self.level == 9:
            print('''
                Your level is {}.
                Is comming, the force!
                '''.format(self.level))
        else:
            print('''
                Your level is {}.
                You have, the force!

                There is no emotion, there is peace.
                There is no ignorance, there is knowledge.
                There is no passion, there is serenity.
                There is no chaos, there is harmony.
                There is no death, there is the Force.
                '''.format(self.level))


class Jedi(Padawan):
    """
    Class that inherits of Padawan.
    """

    def __init__(self, name, lastname):
        """
        Method constructor.
        """

        self.name = name
        self.lastname = lastname
        self.level = 10
        self.force = 'light'
        super().__init__(self.name, self.lastname, 
            level=self.level)


class Sith(Jedi):
    """
    Class that inherits of Jedi but to dark side.
    """

    def __init__(self, name, lastname):
        """
        Method constructor.
        """

        self.name = name
        self.lastname = lastname
        super().__init__(self.name, self.lastname)
        self.force = 'dark'

    def __dark(self):
        print('Dark side!')
```

# Herança

```
anakin = Padawan('Anakin', 'Skywalker')
jedi = Jedi(anakin.name, anakin.lastname)
sith = Sith(jedi.name, jedi.lastname)
sith.name = 'Darth'
sith.lastname = 'Vader'
sith._Sith__dark()
```

# Exercício

>- Refatorar o bot usando Orientação a Objetos

# Obrigado [^0]

![d4n1.org](assets/avatar.jpg){width='44%'}

[^0]: https://d4n1.org
