---
title: Docker
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Maker
>- Hacktivista
>- Músico
>- Motociclista
>- Skatista
>- Geek

# Introdução

>- Virtualização
>- Container

# Docker

![Architecture](assets/arch.png){width='66%'}

# Instalação

```
apt install docker docker.io | docker-ce
```

# Conceitos

>- Imagens
>- Containers
>- Register/Repositories

# Teste

```
docker --version
docker version
docker info
```

# Hello

```
docker run hello-world
docker image ls
docker container ls --all
```

# vim dockerfile

```
FROM python:2.7-slim
WORKDIR /app
COPY . /app
RUN pip install -r requirements.pip
EXPOSE 80
ENV NAME World
CMD ["python", "app.py"]
```

# vim requirements.pip

```
Flask
Redis
```

# vim app.py {.allowframebreaks}

```
from flask import Flask
from redis import Redis, RedisError
import os
import socket

# Connect to Redis
redis = Redis(host="redis", db=0,\
    socket_connect_timeout=2, socket_timeout=2)

app = Flask(__name__)

@app.route("/")
def hello():
    try:
        visits = redis.incr("counter")
    except RedisError:
        visits = "<i>cannot connect to Redis, \
            counter disabled</i>"

    html = "<h3>Hello {name}!</h3>" \
           "<b>Hostname:</b> {hostname}<br/>" \
           "<b>Visits:</b> {visits}"
    return html.format(name=os.getenv("NAME", "world"),
        hostname=socket.gethostname(), visits=visits)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
```

# Build

```
docke build -t app .
```

# Run

```
docker run -d -p 4000:80 app
```

# Share

```
docker login
docker tag app d4n1/app:latest
docker push d4n1/app:latest
```

# Pull

```
docker run -p 4000:80 d4n1/app:latest
```

# Exercício

>- Dockerfile
>- Dockerhub

# Desafio

>- CTF

# Obrigado [^1]

![d4n1](assets/avatar.jpg){width='44%'}

[^1]: http://d4n1.org
