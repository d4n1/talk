---
title: Docker
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# vim docker-compose.py {.allowframebreaks}

```
version: "3"
services:
  web:
    image: username/repo:tag
    deploy:
      replicas: 5
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
      restart_policy:
        condition: on-failure
    ports:
      - "4000:80"
    networks:
      - webnet
networks:
  webnet
```

# Load-balanced

```
docker swarm init
docker stack deploy -c docker-compose.yml getstartedlab
docker service ls
docker stack services getstartedlab
docker service ps getstartedlab_web
docker container ls -q
docker stack ps getstartedlab
curl -4 http://localhost:4000
```

# Scale

```
docker stack deploy -c docker-compose.yml getstartedlab
docker container ls -q
docker stack rm getstartedlab
docker swarm leave --force
```

# Explore

```
docker stack ls
docker stack deploy -c <composefile> <appname>
docker service ls 
docker service ps <service> 
docker inspect <task or container> 
docker container ls -q             
docker stack rm <appname> 
docker swarm leave --force 

```

# Exercício

>- Docker-compose

# Obrigado [^1]

![d4n1](assets/avatar.jpg){width='44%'}

[^1]: http://d4n1.org
