# Debian Easter Egg

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Easter Egg

    !fish
    apt help
    aptitude help
    apt moo
    aptitude moo
    aptitude -v moo
    aptitude -vv moo
    aptitude -vvvvv moo
    aptitude -vvvvvv moo

---

# Easter Egg

    !fish
    apt install fortune cowsay neofetch
    cowsay "hello, I'm a cow"
    fortune | cowsay
    fortune | cowsay -f tux
    cowsay -l
    neofetch

---

# Referências

- [Debian Documentation](https://www.debian.org/doc/)

---

# d4n1.org

![](assets/avatar.png)
