# Debian Env4Dev

---

# Daniel Pimentel

- Mestre em Computação
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Computação
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Agenda

- Debian
- Desktop
- Shell
- Editor

---

# Debian

    !fish
    vim.tiny /etc/apt/sources.list

    deb http://ftp.br.debian.org/debian/ sid main
    deb-src http://ftp.br.debian.org/debian/ sid main


    sudo apt update
    sudo apt full-upgrade
    sudo apt autoremove
    sudo apt autoclean

    apt install sudo
    adduser d4n1 sudo

---

# Desktop

    !fish
    sudo apt install enlightenment terminology connman-ui

---

# Shell

    !fish
    sudo apt install fish    
    curl -L https://get.oh-my.fish | fish
    omf install <theme>


    vim ~/.config/fish/config.fish

    set fish_greeting
    set -U fish_color_normal normal
    set -U fish_color_command ffffff
    set -U fish_color_quote a8a8a8
    set -U fish_color_redirection 808080
    set -U fish_color_end 949494
    set -U fish_color_error 585858
    set -U fish_color_param d7d7d7
    set -U fish_color_selection white --bold --background=brblack
    set -U fish_color_search_match bryellow --background=brblack
    set -U fish_color_history_current --bold
    set -U fish_color_operator 00a6b2
    set -U fish_color_escape 00a6b2
    set -U fish_color_cwd green
    set -U fish_color_cwd_root red
    set -U fish_color_valid_path --underline
    set -U fish_color_autosuggestion 777777
    set -U fish_color_user brgreen
    set -U fish_color_host normal
    set -U fish_color_cancel -r
    set -U fish_pager_color_completion normal
    set -U fish_pager_color_description B3A06D yellow
    set -U fish_pager_color_prefix white --bold --underline
    set -U fish_pager_color_progress brwhite --background=cyan
    set -U fish_color_comment bcbcbc
    set -U fish_color_match --background=brblue
    set -U ECORE_EVAS_ENGINE wayland_shm
    set -U ELM_DISPLAY wl
    set -U ELM_ACCEL none
    set -U TERM xterm-256color

    if test "$XDG_VTNR" = 1
      if test "$DISPLAY" = ''
        enlightenment_start
      end
    end

    alias python=python3
    alias v=vim


    sudo vim /et/passwd

    d4n1:x:1000:1000:d4n1,,,:/home/d4n1:/usr/bin/fish

---

# Editor

    !fish
    sudo apt install vim
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


    vim ~/.vimrc

    call plug#begin("~/.vim/plugged")
      Plug 'joshdick/onedark.vim'
      Plug 'itchyny/lightline.vim'
      Plug 'posva/vim-vue'
      Plug 'mattn/emmet-vim'
    call plug#end()

    syntax on
    set encoding=utf8
    set textwidth=119
    set tabstop=2
    set softtabstop=2
    set shiftwidth=2
    set autoindent
    set expandtab
    set smarttab
    set laststatus=2
    set ignorecase
    set t_Co=256
    set noshowmode
    set cursorline
    set hlsearch
    colorscheme onedark
    let g:lightline = { 'colorscheme': 'onedark' }

---

# Softwares

    !fish
    sudo apt install git docker firefox

---

# Dicas

    !fish
    sudo systemd-analyze blame
    sudo systemctl disable <daemon>
    sudo apt purge <software>

---

# Screenshot

<img src="assets/debian.jpg" width="1024px"/>

---

# References

- [Debian Documentation](https://www.debian.org/doc/)

---

# d4n1.org

![](assets/avatar.png)
