# Debian Expert Install

---

# Daniel Pimentel

- Master in Computing
- Specialist in Software Engineering
- Graduate in Analysis and Development Systems
- Technical in Computing 
- Developer
- Guitarist
- Motorcyclist
- Geek

---

# Motivation

- Minimal
- Time
- Control
- Custom

---

# Boot

![](assets/install.png)

---

# Advanced Options

![](assets/advanced.png)

---

# Main Menu

![](assets/menu.png)

---

# Partition Disks

![](assets/ecrypt.png)

---

# Main Menu

![](assets/base.png)

---

# Base System

![](assets/targeted.png)

---

# Software Selection

![](assets/standard.png)

---

# Main Menu

![](assets/finish.png)

---

# Post Install

- Sid
- Shell
- Desktop
- Others softwares

---

# Screenshot

![](assets/screenshot.jpg)

--- 

# Advantages

- Minimal
- Time
- Control
- Custom

---

# References

- [Debian Documentation](https://www.debian.org/doc/)

---

# d4n1.org

![](assets/avatar.png)
