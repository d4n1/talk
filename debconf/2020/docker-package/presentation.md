# Empacotamento Debian com Docker

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Debian

![](assets/debian.png)

---

# Docker

![](assets/docker.png)

---

# Init

    !fish
    git clone git@gitlab.com:d4n1/package.git

---

# nvim dockerfile

    !docker
    FROM debian:sid

    RUN apt update
    RUN apt install -y autopkgtest blhc devscripts dh-make dput-ng \
        how-can-i-help locales quilt renameutils spell splitpatch \
        git gnupg tree wget neovim

    ADD conf/bashrc /etc/bash.bashrc
    ADD conf/locale.gen /etc/locale.gen
    ADD conf/lintianrc /etc/lintianrc
    ADD conf/devscripts.conf /etc/devscripts.conf
    ADD conf/init.vim /root/.config/nvim/init.vim
    ADD conf/.gnupg /root/.gnupg
    ADD packages /packages

    WORKDIR /packages

    VOLUME packages /packages

---

# nvim conf/bashrc

    !fish
    export DEBFULLNAME="Daniel Pimentel"
    export DEBEMAIL="d4n1@d4n1.org"
    export EDITOR="nvim"
    export LANG=C.UTF-8
    export LANGUAGE=C.UTF-8
    export LC_ALL=C.UTF-8
    export QUILT_PATCHES=debian/patche

---

# nvim conf/locale.gen

    !fish
    en_US.UTF-8 UTF-8

---

# nvim conf/lintianrc

    !fish
    display-info = yes
    pedantic = yes
    display-experimental = yes
    color = auto

---

# nvim conf/devscripts.conf

    !fish
    DEBSIGN_KEYID=D9F9EADD6F26AC782CFCD8317FECE9042C7BFD45

---

# nvim conf/init.vim

    !vim
    syntax on
    set encoding=utf8
    set textwidth=79
    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set autoindent
    set expandtab
    set smarttab
    set laststatus=2
    set ignorecase
    set list
    set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
    hi MatchParen cterm=none ctermbg=gray ctermfg=black

---

# Build

    !fish
    docker build -t debian-package .

---

# Run

    !fish
    docker run -it -v `pwd`/packages:packages debian-package

---

# Destroy

    !fish
    docker images -a | grep "debian-package" | awk '{print $3}' | \
        xargs docker rmi

---

# nvim makefile

    !make
    build:
        docker build -t debian-package .
    run:
        docker run -it -v `pwd`/packages:/packages debian-package
    destroy:
        docker images -a | grep "debian-package" | awk '{print $3}' | \
            xargs docker rmi --force

---

# Referências

- [Debian Documentation](https://www.debian.org/doc/)
- [Guia de Empacotamento Debian](http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)
- [Docker Documentation](https://docs.docker.com/)

---

# d4n1.org

![](assets/avatar.png)
