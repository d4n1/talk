% Debian Packager
% Daniel Pimentel
% DebConf19

# Who I am?

>- Master in Computing
>- Specialist in Software Engineering
>- Graduate in Analysis and Development Systems
>- Technical in Computing
>- Developer
>- Maker
>- Musician
>- Geek

# Packages

>- +59k

# Packager

>- dpkg
>- apt
>- apt-get/apt-cache
>- aptitude
>- synaptic

# People

>- Packager
>- Debian Maintainer
>- Debian Developer
>- Sponsor
>- Leader

# Developers

![DDs arround the world](assets/developers.jpg){width=93%}

# Concepts

>- Upstream tarball
>- Source package
>- Binary package

# Init

```
mkdir sid
sudo debootstrap sid sid http://ftp.br.debian.org/debian
sudo chroot sid
```

# Init (chroot)

```
echo "/proc proc/ proc defaults 0 0" >> /etc/fstab
mount /proc
apt update
apt install vim wget
```

# Environment (chroot)

```
vim ~/.bashrc
export DEBFULLNAME="Daniel Pimentel"
export DEBEMAIL="d4n1@d4n1.org"
export EDITOR=vim
export QUILT_PATCHES=debian/patches
export QUILT_DIFF_ARGS="--no-timestamps --no-index -pab"
export QUILT_REFRESH_ARGS="--no-timestamps --no-index \
    -pab"
```

# Settings (chroot)

```
apt install autopkgtest blhc devscripts dh-make dput-ng \
    how-can-i-help locales quilt renameutils spell \
    splitpatch tree 
dpkg-reconfigure locales 
dpkg-reconfigure tzdata
```

# Settings (chroot)

```
vim /etc/lintianrc
display-info = yes
pedantic = yes
display-experimental = yes
```

# GPG

```
gpg --full-gen-key
gpg --armor --export d4n1@d4n1.org > publickey
gpg --armor --export-secret-keys d4n1@d4n1.org \ 
    > privatekey
sudo cp publickey privatekey <path-chroot-sid>
```

# GPG (chroot)

```
echo "pinentry-mode loopback" >>  ~/.gnupg/gpg.conf
gpg --full-gen-key
gpg --list-keys
gpg --import publickey privatekey
rm publickkey privatekey
```

# GPG (chroot)

```
vim /etc/devscripts.conf
DEBSIGN_KEYID=<key>
```

# Backup

```
tar -cvjf sid.tar.bz --exclude='*/proc/*' \
    --exclude='*/apt/lists/*dists*' /sid
```

# Build (chroot)

```
apt search <upstream>
wnpp-search <upstream>
wget <upstreal-url>
tar -xvf upstream-1.0.tar.xz
cd upstream-1.0
licensecheck -r *
egrep -sriA25 '(copyright|public dom)'
dh_make -f ../upstream-1.0.tar.xz -c <license>
```

# Packaging (chroot)

```
cd debian
mv compat <upstream>.* manpage.* menu.* post* pre* README.* watch* *.docs /tmp
nvim copyright changelog control rules format watch
```

# Build (chroot)

```
debuild -S
```

# Lintian (chroot)

```
lintian -i
```

# Scan (chroot)

```
uscan
```

# Hardening (chroot)

```
blhc <upstream.build>
```

# Install (chroot)

```
debi
```

# Test (chroot)

```
mkdir debian/test
nvim debian/test/control
autopkgtest . -- null
```

# Check

```
apt install cowbuilder
cowbuilder create
cowbuilder build <upstream.dsc>
```

# Send (chroot)

```
dput mentors <upstream.changes>
```

# Update (chroot)

```
uupdate ../<upstream-tarbal>
```

# Report (chroot)

```
reportbug -b
```

# Tips

>- apt-cacher-ng
>- gpb

# Mainlist

>- debian-announce
>- debian-devel-announce
>- debian-infrastructure-announce
>- debian-legal
>- debian-mentors
>- debian-br-eventos

# Site

>- [Tracker](https://tracker.debian.org)
>- [Bug](https://bugs.debian.org)
>- [Packages](https://packages.debian.org)
>- [Lintian](https://lintian.debian.org)
>- [Mentors](https://mentors.debian.net)

# Help

>- Documentation
>- Translate
>- Packaging
>- Bugs
>- Artwork
>- Mirrors
>- Donations
>- Share :)

# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Thanks [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
