%title: DebPack (Debian Package)
%author: Daniel (d4n1)
%date: 2023-08-26

-> DebPack <-

-> Daniel (d4n1) <-

----

-> Whoami <-

<br>
- Master in Computing
<br>
- Specialist in Software Engineering
<br>
- Graduate in Analysis and Development Systems
<br>
- Technical in Computing
<br>
- Developer
<br>
- Biker
<br>
- Musician
<br>
- Geek

----
-> Packages <-

<br>
- +60k

----

-> Package Manager <-

<br>
- dpkg
<br>
- apt
<br>
- aptitude
<br>
- synaptic

----

-> Developers <-

<br>
- Packager
<br>
- Debian Maintainer
<br>
- Debian Developer
<br>
- Sponsor
<br>
- Leader

----

-> Requirements <-

<br>
~~~~
sudo apt install docker git gnupg make
git clone git@gitlab.com:d4n1/debpack-template.git
~~~~

----

-> Init <-

<br>
~~~~
gpg --list-keys # gpg --full-gen-key
cp -r ~/.gnupg conf/gnupg
nvim conf/devscripits.conf # <key>
nvim conf/bashrc # <author>, <email>
nvim dockerfile # <user>
nvim makefile # <user>
~~~~

----

-> Build <-

<br>
~~~~
make build
~~~~

----

-> Run <-

<br>
~~~~
make run
~~~~

----

-> Destroy <-

<br>
~~~~
make destroy
~~~~

----

-> Thanks :) <-


[d4n1](https://d4n1.org)
