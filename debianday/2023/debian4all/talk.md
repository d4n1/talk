%title: Debian, O Sistema Operacional Universal
%author: Daniel (d4n1)
%date: 2023-10-11

-> Debian <-

-> Daniel (d4n1) <-

----

-> Whoami <-

<br>
- Mestre em Computação
<br>
- Especialista em Engenharia de Software
<br>
- Graduado em Análise e Desenvolvimento de Sistemas
<br>
- Técnico em Computação
<br>
- Desenvolvedor
<br>
- Motoqueiro
<br>
- Guitarrista
<br>
- Geek

----

-> Início <-

<br>
- Carta de Ian Murdock em 1993

----

-> Nome <-

<br>
- **Deb**\ra Lynn + **Ian** Murdock

----

-> Logo <-

<br>
- Magic smoke
<br>
- Covinha do Buzz

----

-> Base <-

<br>
- Software Livre
<br>
- Contrato Social
<br>
- Manifesto Debian

----

-> Software Livre <-

<br>
- Software Livre
<br>
- A liberdade de executar o programa
<br>
- A liberdade de estudar o programa
<br>
- A liberdade de redistribuir cópias do programa
<br>
- A liberdade de modiﬁcar o programa e redistribuir

----

-> Contrato Social <-

<br>
- O Debian permanecerá 100% livre
<br>
- Nós iremos retribuir à comunidade software livre
<br>
- Nós não esconderemos problemas
<br>
- Nossas prioridades são nossos usuários e o software livre
<br>
- Programas que não atendem nossos padrões de software livre

----

-> Manifesto Debian <-

<br>
- Redistribuição livre
<br>
- Código fonte
<br>
- Trabalhos derivados
<br>
- Integridade do código fonte do autor
<br>
- Não à discriminação contra pessoas ou grupos
<br>
- Não à discriminação contra ﬁns de utilização
<br>
- Distribuição de licença
<br>
- A Licença não pode ser especíﬁca para o Debian
<br>
- A Licença não deve contaminar outros softwares

----

-> Versões <-

<br>
- Unstable
<br>
- Testing
<br>
- Stable

----

-> Versões <-

<br>
- 1.1 Buzz
<br>
- 2 Hamm
<br>
- 2.1 Slink
<br>
- 2.2 Potato
<br>
- 3 Woody
<br>
- 3.1 Sarge
<br>
- 4 Etch
<br>
- 5 Lenny
<br>
- 6 Squeeze
<br>
- 7 Wheezy
<br>
- 8 Jessie
<br>
- 9 Stretch
<br>
- 10 Buster 
<br>
- 11 Bullseye
<br>
- 12 Bookworm

----

-> Kernel <-

<br>
- Linux
<br>
- KFreeBSD
<br>
- Hurd

----

-> Hardware <-

<br>
- 64-bit PC (amd64)
<br>
- 32-bit PC (i386)
<br>
- EABI ARM (armel)
<br>
- Hard Float ABI ARM (armhf)
<br>
- MIPS (little endian)
<br>
- MIPS (big endian)
<br>
- IBM System z
<br>
- 64-bit ARM (AArch64)
<br>
- POWER Processors
<br>
- 64-bit MIPS (little endian) 

----

-> Líder <-

<br>
- Ian Murdock (1993-1996)
<br>
- Bruce Perens (1996-1998)
<br>
- Ian Jackson (1998-1999)
<br>
- Wichert Akkerman (1999-2001)
<br>
- Ben Collins (2001-2002)
<br>
- Bdale Garbee (2002-2003)
<br>
- Martin Michlmayr (2003-2005)
<br>
- Branden Robinson (2005-2006)
<br>
- Anthony Towns (2006-2007)
<br>
- Sam Hocevar (2007-2008)
<br>
- Steve Mcintyre (2008-2010)
<br>
- Stefano Zacchiroli (2010-2013)
<br>
- Lucas Nussbaum (2013-2015)
<br>
- Neil McGovem (2015-2016)
<br>
- Mehdi Dogguy (2016-2017)
<br>
- Chris Lamb (2017-2019)
<br>
- Sam Hartman (2019-2020)
<br>
- Jonathan Carter (atual)

----

-> Pacotes <-

<br>
- +60k

----

-> Package Manager <-

<br>
- dpkg
<br>
- apt
<br>
- aptitude
<br>
- synaptic

----

-> Desenvolvedores <-

<br>
- Packager
<br>
- Debian Maintainer
<br>
- Debian Developer
<br>
- Sponsor
<br>
- Leader

----

-> Distribuições Baseadas <-

<br>
- Ubuntu
<br>
- Backtrack
<br>
- Kali Linux
<br>
- Linux Mint
<br>
- PureOS
<br>
- Kurumin Linux
<br>
- ...

----

-> Quem usa? <-

<br>
- Instituições educacionais
<br>
- Empresas
<br>
- ONGs
<br>
- Governo

----

-> Divertido <-

<br>
~~~~
apt moo

apt install fortune cowsay

cowsay "Hello, Debian"

fortune | cowsay

fortune | cowsay -f tux

cowsay -l

fortune | cowsay -f stegosaurus

nmap -oS - scanme.nmap.org

nvim
:help 42
~~~~

----

-> Obrigado :) <-


[d4n1](https://d4n1.org)
