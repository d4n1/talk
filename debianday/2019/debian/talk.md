% Debian Packager
% Daniel Pimentel
% DebianDay

# Who I am?

>- Master in Computing
>- Specialist in Software Engineering
>- Graduate in Analysis and Development Systems
>- Technical in Computing
>- Developer
>- Maker
>- Musician
>- Geek

# Packages

>- +59k

# Packager

>- dpkg
>- apt
>- apt-get/apt-cache
>- aptitude
>- synaptic

# People

>- Packager
>- Debian Maintainer
>- Debian Developer
>- Sponsor
>- Leader

# Developers

![DDs arround the world](assets/developers.jpg){width=93%}

# Concepts

>- Upstream tarball
>- Source package
>- Binary package

# Init

```
mkdir sid
sudo debootstrap sid sid http://ftp.br.debian.org/debian
sudo chroot sid
```

# Init (chroot)

```
echo "/proc proc/ proc defaults 0 0" >> /etc/fstab
mount /proc
apt update
apt install autopkgtest blhc devscripts dh-make dput-ng \
    how-can-i-help locales quilt renameutils spell \
    splitpatch tree wget vim

```

# Environment (chroot)

```
vim ~/.bashrc

export DEBFULLNAME="Daniel Pimentel"
export DEBEMAIL="d4n1@d4n1.org"
export EDITOR=vim
export QUILT_PATCHES=debian/patches
export QUILT_DIFF_ARGS="--no-timestamps --no-index -pab"
export QUILT_REFRESH_ARGS="--no-timestamps --no-index \
    -pab"
```

# Settings (chroot)

```
dpkg-reconfigure locales 
dpkg-reconfigure tzdata
```

# Settings (chroot)

```
vim /etc/lintianrc
display-info = yes
pedantic = yes
display-experimental = yes
```

# GPG

```
gpg --full-gen-key
gpg --armor --export d4n1@d4n1.org > publickey
gpg --armor --export-secret-keys d4n1@d4n1.org \ 
    > privatekey
sudo cp publickey privatekey <path-chroot-sid>
```

# GPG (chroot)

```
echo "pinentry-mode loopback" >>  ~/.gnupg/gpg.conf
gpg --full-gen-key
gpg --list-keys
gpg --import publickey privatekey
rm publickkey privatekey
```

# GPG (chroot)

```
vim /etc/devscripts.conf
DEBSIGN_KEYID=<key>
```

# Backup

```
tar -cvjf sid.tar.bz --exclude='*/proc/*' \
    --exclude='*/apt/lists/*dists*' /sid
```

# Search (chroot)

```
apt search <upstream>
wnpp-check <upstream>
```

# Get (chroot)

```
wget <upstreal-url>
tar -xvf upstream-1.0.tar.xz
cd upstream-1.0
```

# License

```
licensecheck --deb-machine --merge-licenses -r *
egrep -sriA25 '(copyright|public dom)'
egrep '(@|\[)' <changelog>
```

# Make

```
dh_make -f ../upstream-1.0.tar.xz -c <license>
```

# Packaging (chroot)

```
cd debian
mv compat <upstream>.* manpage.* menu.* post* pre* \
    README.* watch* *.docs /tmp
vim changelog copyright control rules source/format \
    tests/control watch
```

# Build (chroot)

```
cd ..
debuild 
```

# Lintian (chroot)

```
lintian -i
```

# Scan (chroot)

```
uscan
```

# Hardening (chroot)

```
blhc --all --debian <upstream.build>
```

# Install (chroot)

```
debi
```

# Test (chroot)

```
mkdir debian/test
vim debian/test/control
autopkgtest . -- null
```

# Check

```
apt install cowbuilder
cowbuilder create
cowbuilder build <upstream.dsc>
```

# Send (chroot)

```
dput mentors <upstream.changes>
```

# Changelog (chroot)

```
dch -i
debuild
```

# Check (chroot)

```
debdiff <upstream-1.dsc> <upstream-2.dsc>
```

# Update (chroot)

```
uupdate ../<upstream-tarbal>
```

# Patch (chroot)

```
vim <file>
dpkg-source --commit
quilt push -a
quilt pop -a
```

# Report (chroot)

```
reportbug -b
```

# QA

```
apt source <package>
dch --qa
vim debian/control
apt build-dep <package>
```

# NMU

```
dch --nmu
dput -e <day> <changes>
nmudiff --no-mutt
dcut -k <key> cancel <changes> 
```

# GBP

```
gbp export-origin

git clone <repo>
cd <upstream>
git checkout upstream
git checkout debian/master

gbp import-orig ../<upstream>
gbp dch
apt build-deps <upstream>
debuild
gbp tag
gbp import-dsc <dsc>

gbp import-dscs --debsnap <package>
```

# GBP

```
vim ~/.gbp.conf

[DEFAULT]
git-debian--branch= debian/master

[GBP]
pristine-tar=True
git-log=--no-merges
tar <new>
gbp import-orig ../<new>
gbp dch
git commit -m '<msg>'
debuild
git push --all --tag
```

# Mainlist

>- debian-announce
>- debian-devel-announce
>- debian-infrastructure-announce
>- debian-legal
>- debian-mentors
>- debian-br-eventos

# Site

>- [Tracker](https://tracker.debian.org)
>- [Bug](https://bugs.debian.org)
>- [Packages](https://packages.debian.org)
>- [Lintian](https://lintian.debian.org)
>- [Mentors](https://mentors.debian.net)
>- [Snapshot](https://snapshot.debian.org)

# Help

>- Documentation
>- Translate
>- Packaging
>- Bugs
>- Artwork
>- Mirrors
>- Donations
>- Share :)

# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Thanks [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
