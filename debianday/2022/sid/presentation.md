# Debian "Rolling Release" (Sid)

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Início


![Carta de Ian Murdock em 93](assets/ian.jpg)

---

# Nome

- **Deb**ra Lynn
- **Ian** Murdock

---

# Logo


![Magic Smoke?](assets/logo.png)

---

# Base

- Software Livre
- Contrato Social
- Manifesto Debian

---

# Software Livre

- A **liberdade de executar** o programa
- A **liberdade de estudar** o programa
- A **liberdade de redistribuir** cópias do programa
- A **liberdade de modificar** o programa e redistribuir

---

# Contrato Social

- O Debian permanecerá 100% livre
- Nós iremos retribuir à comunidade de software livre
- Nós não esconderemos problemas
- Nossas prioridades são nossos usuários e o software livre
- Programas devem atender nossos padrões de software livre

---

# Manifesto Debian

- Redistribuição livre
- Código fonte
- Trabalhos derivados
- Integridade do código fonte do autor
- Não à discriminação contra pessoas ou grupos
- Não à discriminação contra fins de utilização
- Distribuição de licença
- A Licença não pode ser específica para o Debian
- A Licença não deve contaminar outros softwares

---

# Versões

- Unstable
- Testing
- Stable

---

# Fluxo

![](assets/flow.png)

---

# Releases

    !bash
    1.1 Buzz
    2.0 Hamm
    2.1 Slink
    2.2 Potato
    3.0 Woody
    3.1 Sarge
    4.0 Etch
    5.0 Lenny
    6.0 Squeeze
    7.0 Wheezy
    8.0 Jessie
    9.0 Stretch
    10.0 Buster
    11.0 Bulleye

---

# Kernel

- Linux
- KfreeBSD
- Hurd

---

# Hardware

- amd64 (x86-64 64-bit)
- arm64 (ARMv8-A)
- armel (Little-endian ARM)
- armhf (ARMv7)
- i386 (IA-32 32-bit)
- mips64el (Little-endian 64-bit MIPS)
- mipsel (Little-endian 32-bit MIPS)
- ppc64el (Little-endian PowerPC)
- s390x (z/Architecture 64-bit)

---

# Líderes

    !bash
    Ian Murdock (1993-1996)
    Bruce Perens (1996-1998)
    Ian Jackson (1998-1999)
    Wichert Akkerman (1999-2001)
    Ben Collins (2001-2002)
    Bdale Garbee (2002-2003)
    Martin Michlmayr (2003-2005)
    Branden Robinson (2005-2006)
    Anthony Towns (2006-2007)
    Sam Hocevar (2007-2008)
    Steve Mcintyre (2008-2010)
    Stefano Zacchiroli (2010-2013)
    Lucas Nussbaum (2013-2015)
    Neil McGovem (2015-2016)
    Mehdi Dogguy (2016-2017)
    Chris Lamb (2018-2019)
    Sam Hartman (2020)
    Jonathan Carter (atual)

---

# Desenvolvedores Debian

![](assets/developers.jpg)

---

# Pacotes

- dpkg (Debian Package)
- apt (Advanced Packaging Tool)
- apt-get e apt-cache, aptitude, synaptic
- +59k

---

# Distribuições Baseadas

![](assets/tree.jpg)

---

# Sid (Unstable)

- Pacotes atuais
- Últimas novidades
- Futura release
- Instávelmente estável
- Sem gerenciamento do Security Team
- Use por sua conta e risco

---

# nvim /etc/apt/source.list

    !bash
    deb http://deb.debian.org/debian/ sid main
    deb-src http://deb.debian.org/debian/ sid main
    apt update && apt full-upgrade
    reboot

---

# Referências

- [Debian](https://debian.org/)

---

# d4n1.org

![](assets/avatar.png)
