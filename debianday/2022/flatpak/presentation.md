# Flatpak

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Package

- Código
- Metadata

---

# Flatpak

- Independente
- Repositórios remotos
- Flathub
- Sandbox
- Mais espaço
- Melhor para os Desenvolvedores

---

# Snap

- Canonical
- Múltiplas versões
- SnapStore
- Mais lento

---

# AppImage

- Sem instalação
- Sem permissão especiais
- Um arquivo por App
- AppImageHub
- Mais rápido

---

# Init

    !bash
    apt install flatpak
    flatpak remotes
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

---

# Search Package

    !bash
    flatpak search firefox

---

# Install Package

    !bash
    flatpak install flathub org.mozilla.firefox

---

# Run Package
  
    !bash
    flatpak run org.mozilla.firefox

---

# Update Packages

    !bash
    flatpak update

---

# List Packages

    !bash
    flatpak list

---

# Remove Package

    !bash
    flatpak uninstall org.mozilla.firefox

---

# Troubleshooting

    !bash
    flatpak uninstall --unused
    flatpak repair
    flatpak permission-reset org.mozilla.firefox


---

# Install SDK

    !bash
    flatpak install flathub org.freedesktop.Platform//21.08 org.freedesktop.Sdk//21.08

---

# nvim hello.sh

    !vim
    #!/bin/sh
    echo "Hello world, from a sandbox"

---

# nvim org.flatpak.Hello.yml

    !vim
    app-id: org.flatpak.Hello
    runtime: org.freedesktop.Platform
    runtime-version: '21.08'
    sdk: org.freedesktop.Sdk
    command: hello.sh
    modules:
      - name: hello
        buildsystem: simple
        build-commands:
          - install -D hello.sh /app/bin/hello.sh
       sources:
          - type: file
            path: hello.sh

---

# Build App

    !bash
    flatpak-builder build-dir org.flatpak.Hello.yml
    flatpak-builder --user --install --force-clean build-dir org.flatpak.Hello.yml
    flatpak run org.flatpak.Hello

---

# Add App Repository

    !bash
    flatpak-builder --repo=repo --force-clean build-dir org.flatpak.Hello.yml

---

# Install App

    !bash
    flatpak --user remote-add --no-gpg-verify tutorial-repo repo
    flatpak --user install tutorial-repo org.flatpak.Hello

---

# Run App

    !bash
    flatpak run org.flatpak.Hello

---

# Referências

- [Flatpak](https://flatpak.org/)

---

# d4n1.org

![](assets/avatar.png)
