---
title: Debian
author: Daniel Pimentel
institute: Debian Day
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Professor
>- Pesquisador
>- Músico
>- Geek

# Início

![Carta de Ian Murdock em 93](assets/ian.jpg){width=44%}

# Nome

>- **Deb**ra Lynn
>- **Ian** Murdock

# Logo

![A logo **redemoinho** representa a magic smoke](assets/logo.png){width=44%}

# Orgiem

>- Software Livre
>- Contrato Social
>- Manifesto Debian

# Software Livre

>- A liberdade de executar o programa
>- A liberdade de estudar o programa
>- A liberdade de redistribuir cópias do programa
>- A liberdade de modificar o programa e redistribuir

# Contrato Social

>- O Debian permanecerá 100% livre
>- Nós iremos retribuir à comunidade software livre
>- Nós não esconderemos problemas
>- Nossas prioridades são nossos usuários e o software livre
>- Programas que não atendem nossos padrões de software livre

# Manifesto Debian

>- Redistribuição livre
>- Código fonte
>- Trabalhos derivados
>- Integridade do código fonte do autor
>- Não à discriminação contra pessoas ou grupos
>- Não à discriminação contra fins de utilização
>- Distribuição de licença
>- A Licença não pode ser específica para o Debian
>- A Licença não deve contaminar outros softwares

# Versões

>- Unstable
>- Testing
>- Stable

# Releases

>- 1.1 Buzz
>- 2 Hamm
>- 2.1 Slink
>- 2.2 Potato
>- 3 Woody
>- 3.1 Sarge
>- 4 Etch
>- 5 Lenny
>- 6 Squeeze
>- 7 Wheezy
>- 8 Jessie
>- 9 Stretch
>- *10 Buster*

# Kernel

>- Linux
>- KfreeBSD
>- Hurd

# Hardware

>- 64-bit PC (amd64)
>- 32-bit PC (i386)
>- EABI ARM (armel)
>- Hard Float ABI ARM (armhf)
>- MIPS (little endian)
>- MIPS (big endian)
>- IBM System z
>- 64-bit ARM (AArch64)
>- POWER Processors
>- 64-bit MIPS (little endian) 

# Líderes

>- Ian Murdock (1993-1996)
>- Bruce Perens (1996-1998)
>- Ian Jackson (1998-1999)
>- Wichert Akkerman (1999-2001)
>- Ben Collins (2001-2002)
>- Bdale Garbee (2002-2003)
>- Martin Michlmayr (2003-2005)
>- Branden Robinson (2005-2006)
>- Anthony Towns (2006-2007)
>- Sam Hocevar (2007-2008)
>- Steve Mcintyre (2008-2010)
>- Stefano Zacchiroli (2010-2013)
>- Lucas Nussbaum (2013-2015)
>- Neil McGovem (2015-2016)
>- Mehdi Dogguy (2016-2017)
>- Chris Lamb (atual)

# Desenvolvedores

![Desenvolvedores Debian](assets/developers.jpg){width=93%}

# Pacotes

>- dpkg (Debian Package)
>- apt (Advanced Packaging Tool)
>- apt-get e apt-cache, aptitude, synaptic
>- +51k

# Familia

![Árvore da família Debian](assets/tree.jpg){width=30%}

# Ajuda

>- Documentação
>- Wiki
>- Mailing lists
>- Forums
>- Bug tracking
>- Redes Sociais

# Referências

>- https://www.debian.org/doc/

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
