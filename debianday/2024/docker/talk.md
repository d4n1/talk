%title: Oxe, Debian Container?
%author: Daniel (d4n1)
%date: 2024-08-17
%theme: dark

-> Oxe, Debian Container? <-

----

-> Whoami <-

<br>
- Master in Computing (UFAL)
<br>
- Free Software since ~2000 (GNU, Debian, ...)
<br>
- Speaker since ~2010 (DebConf, FISL ...)
<br>
- Developer since ~2000 (BackEnd, DevOps ...)
<br>
- Biker (live to ride, ride to live)
<br>
- Musician (guitar)
<br>
- Geek :)

----

-> Debian <-

<br>
- Free Software
<br>
- Social Contract
<br>
- Stable
<br>
- Secure
<br>
- Hardware support (+10)
<br>
- Packages (+60k)
<br>
- Community

----

-> Virtualization x Container <-

<br>
- Virtualization
<br>
- Containers

----

-> Chroot  <-

<br>
~~~~
apt install debootstrap
mkdir -p ~/debian
debootstrap bullseye ~/debian http://deb.debian.org/debian
chroot ~/debian
~~~~

----

-> LXC <-

<br>
~~~~
apt install lxc lxctl lxc-templates
lxc-create -n debian -t debian -- -r bullseye
lxc-start --name debian
lxc-info --name debian
lxc-stop --name debian
~~~~

----

-> Docker <-

<br>
~~~~
apt install ca-certificates curl
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update
apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

docker run hello-world
~~~~

----

-> Show me the code <-

<br>
```
docker build -t <container> .
docker run -p <port>:5000 -t <continer>
wrk -t <x> -c <y> -d <z> http://localhost:<port> --latency
```

---

-> Benchmark Debian (-t 4 -c 8000 -d 60) <-

<br>
```
Running 1m test @ http://localhost:5000
  4 threads and 8000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   806.71ms  437.99ms   1.92s    78.55%
    Req/Sec    34.83     17.79   170.00     75.22%
  Latency Distribution
     50%  684.71ms
     75%  920.97ms
     90%    1.63s
     99%    1.89s
  7955 requests in 1.00m, 1.42MB read
  Socket errors: connect 6983, read 0, write 0, timeout 7680
  Non-2xx or 3xx responses: 8
Requests/sec:    132.38
Transfer/sec:     24.21KB
```

---

-> Benchmark Alpine (-t 4 -c 8000 -d 60) <-

<br>
```
Running 1m test @ http://localhost:5001
  4 threads and 8000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.11s   454.08ms   1.99s    65.62%
    Req/Sec    35.46     16.88   100.00     76.40%
  Latency Distribution
     50%  965.81ms
     75%    1.49s
     90%    1.83s
     99%    1.98s
  8172 requests in 1.00m, 1.46MB read
  Socket errors: connect 6983, read 0, write 0, timeout 7980
  Non-2xx or 3xx responses: 3
Requests/sec:    136.00
Transfer/sec:     24.85KB
```

---

-> Benchmark Debian (-t 4 -c 8000 -d 1800) <-

<br>
```
Running 30m test @ http://localhost:5000
  4 threads and 8000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.20s   663.89ms   2.00s    59.97%
    Req/Sec    34.60     16.37   140.00     77.15%
  Latency Distribution
     50%    1.39s
     75%    1.82s
     90%    1.94s
     99%    1.99s
  245364 requests in 30.00m, 43.78MB read
  Socket errors: connect 6983, read 0, write 0, timeout 244687
  Non-2xx or 3xx responses: 92
Requests/sec:    136.31
Transfer/sec:     24.91KB
```

---

-> Benchmark Alpine (-t 4 -c 8000 -d 1800) <-

<br>
```
Running 30m test @ http://localhost:5001
  4 threads and 8000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.39s   666.50ms   2.00s    81.88%
    Req/Sec    35.52     16.50   121.00     76.30%
  Latency Distribution
     50%    1.66s
     75%    1.86s
     90%    1.95s
     99%    1.99s
  252246 requests in 30.00m, 45.01MB read
  Socket errors: connect 6983, read 0, write 0, timeout 251788
  Non-2xx or 3xx responses: 80
Requests/sec:    140.13
Transfer/sec:     25.60KB
```

---

-> Benchmark Debian (-t 4 -c 800000 -d 1800) <-

<br>
```
Running 30m test @ http://localhost:5000
  4 threads and 800000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.27s   756.96ms   2.00s    74.10%
    Req/Sec    34.51     16.64   121.00     76.22%
  Latency Distribution
     50%    1.63s
     75%    1.88s
     90%    1.95s
     99%    1.99s
  243877 requests in 30.00m, 43.60MB read
  Socket errors: connect 798983, read 0, write 0, timeout 243348
  Non-2xx or 3xx responses: 406
Requests/sec:    135.48
Transfer/sec:     24.80KB
```

---

-> Benchmark Alpine (-t 4 -c 800000 -d 1800) <-

<br>
```
Running 30m test @ http://localhost:5001
  4 threads and 800000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.37s   650.96ms   2.00s    82.07%
    Req/Sec    36.30     16.89   130.00     75.21%
  Latency Distribution
     50%    1.61s
     75%    1.84s
     90%    1.93s
     99%    1.99s
  257689 requests in 30.01m, 46.06MB read
  Socket errors: connect 798983, read 0, write 0, timeout 257014
  Non-2xx or 3xx responses: 402
Requests/sec:    143.14
Transfer/sec:     26.20KB
```

---

-> Benchmark Alpine (-t 4 -c 800000 -d 3600) <-

<br>
```
Running 60m test @ http://localhost:5000
  4 threads and 800000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.33s   690.12ms   2.00s    79.75%
    Req/Sec    35.01     16.94   130.00     75.42%
  Latency Distribution
     50%    1.63s
     75%    1.87s
     90%    1.94s
     99%    1.99s
  495264 requests in 60.00m, 88.49MB read
  Socket errors: connect 798983, read 0, write 0, timeout 494242
  Non-2xx or 3xx responses: 640
Requests/sec:    137.57
Transfer/sec:     25.17KB
```

---

-> Benchmark Alpine (-t 4 -c 800000 -d 3600) <-

<br>
```
Running 60m test @ http://localhost:5001
  4 threads and 800000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.48s   580.39ms   2.00s    86.64%
    Req/Sec    36.40     16.46   120.00     76.66%
  Latency Distribution
     50%    1.70s
     75%    1.87s
     90%    1.95s
     99%    1.99s
  518362 requests in 60.00m, 92.57MB read
  Socket errors: connect 798983, read 0, write 0, timeout 517269
  Non-2xx or 3xx responses: 490
Requests/sec:    143.98
Transfer/sec:     26.33KB
```

---

-> Oxe, Debian Container? <-

<br>
- Base image
<br>
- Stable
<br>
- Minimal
<br>
- Performance
<br>
- Compatibility
<br>
- Packages
<br>
- Community

---

-> Documentation <-

<br>
- [Debian](https://debian.org)
<br>
- [LXC](https://linuxcontainers.org/)
<br>
- [Docker](https://docker.com)

----

-> Thanks :) <-

[d4n1](https://d4n1.org)
