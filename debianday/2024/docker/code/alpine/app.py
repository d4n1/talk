import cProfile
import re

from flask import Flask


app = Flask(__name__)

@app.route("/")
def hello_world():
    cProfile.run('re.compile("foo|bar")')

    return "Hello, Debian!"

if __name__ == '__main__':
	app.run(host='0.0.0.0')
