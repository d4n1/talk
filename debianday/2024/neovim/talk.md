%title: NeoVim
%author: Daniel (d4n1)
%date: 2024-08-16
%theme: dark

-> Neovim <-

----

-> Whoami <-

<br>
- Master in Computing (UFAL)
<br>
- Free Software since ~2000 (GNU, Debian, ...)
<br>
- Speaker since ~2010 (DebConf, FISL ...)
<br>
- Developer since ~2000 (BackEnd, DevOps ...)
<br>
- Biker (live to ride, ride to live :)
<br>
- Musician (\m/)
<br>
- Geek :)

----

-> Neovim <-

<br>
- Vim fork
<br>
- APIs
<br>
- Plugins

----

-> Goals <-

<br>
- Vim like
<br>
- Contributors
<br>
- Unblock plugin authors
<br>
- First-class Lua interface
<br>
- Optimize "out of the box"
<br>
- Cross-platform experience

----

-> Non-goals <-

<br>
- Support Vim9script
<br>
- Vim IDE
<br>
- Deprecate Vimscript
<br>
- POSIX vi

----

-> Neovim on Debian <-

<br>
- Stable: 0.7.x 
<br>
- Sid: **0.9.x**

----

-> Requirements <-

<br>
- Neovim
<br>
- Git
<br>
- Packer

----

-> Install <-

<br>
~~~~
sudo apt install neovim git
git clone --depth 1 https://github.com/wbthomason/packer.nvim \
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
~~~~

----

-> Init <-

<br>
~~~~
cd ~/.config/nvim 
touch init.lua
mkdir -p lua/configs
touch lua/settings.lua
touch lua/keymaps.lua 
touch lua/plugins.lua
touch lua/configs/<plugin>.lua
~~~~

----

-> Run <-

<br>
~~~~
nvim init.lua
:PackerSync
~~~~

----

-> Thanks :) <-


[d4n1](https://d4n1.org)
