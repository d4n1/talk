\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Sobre Mim}{4}{0}{1}
\beamer@sectionintoc {2}{Criptografia}{17}{0}{2}
\beamer@subsectionintoc {2}{1}{Criptografia}{17}{0}{2}
\beamer@subsectionintoc {2}{2}{Comunica\IeC {\c c}\IeC {\~a}o}{19}{0}{2}
\beamer@subsectionintoc {2}{3}{Terminologia}{23}{0}{2}
\beamer@subsectionintoc {2}{4}{Termos}{25}{0}{2}
\beamer@subsectionintoc {2}{5}{Tipos de Criptografia}{35}{0}{2}
\beamer@subsectionintoc {2}{6}{Hash}{43}{0}{2}
\beamer@subsectionintoc {2}{7}{Assinatura Digital}{50}{0}{2}
\beamer@subsectionintoc {2}{8}{Certificado Digital}{56}{0}{2}
\beamer@sectionintoc {3}{Privacidade}{65}{0}{3}
\beamer@subsectionintoc {3}{1}{Vigil\IeC {\^a}ncia em Massa}{65}{0}{3}
\beamer@sectionintoc {4}{Software Livre}{67}{0}{4}
\beamer@subsectionintoc {4}{1}{FSF}{67}{0}{4}
\beamer@subsectionintoc {4}{2}{4 Liberdades}{69}{0}{4}
\beamer@subsectionintoc {4}{3}{GNU}{74}{0}{4}
\beamer@sectionintoc {5}{Criptografia Livre}{76}{0}{5}
\beamer@subsectionintoc {5}{1}{Sofwares Livres Criptogr\IeC {\'a}ficos}{76}{0}{5}
\beamer@subsectionintoc {5}{2}{GnuPG}{85}{0}{5}
\beamer@subsectionintoc {5}{3}{Caracter\IeC {\'\i }sticas do GnuPG}{87}{0}{5}
\beamer@subsectionintoc {5}{4}{Usando GnuPG}{95}{0}{5}
\beamer@subsectionintoc {5}{5}{Criptografar Arquivo com GnuPG}{97}{0}{5}
\beamer@subsectionintoc {5}{6}{Manipular Hash com GnuPG}{98}{0}{5}
\beamer@subsectionintoc {5}{7}{Criptografar Email com GnuPG}{99}{0}{5}
\beamer@sectionintoc {6}{Dicas}{100}{0}{6}
\beamer@sectionintoc {7}{Refer\IeC {\^e}ncias}{107}{0}{7}
\beamer@sectionintoc {8}{Fim}{108}{0}{8}
