#Gerar 
gpg -gen-key

#touch "pinentry-program path/pinentry-curses" >
#~./gnupg/gpg-agent.conf && killall gpg-agent 

# cat /dev/urandom > /dev/null

#Listar
gpg --list-key
gpg --list-keys

#Assinar
gpg --sign-key

#Fingerprint
gpg --fingerprint

#Exportar
gpg --export --armor Daniel

#Help :)
gpg --help
