\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{About Me}{3}{0}{1}
\beamer@sectionintoc {2}{Guile}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{About}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Features}{8}{0}{2}
\beamer@subsectionintoc {2}{3}{Programming Languages Supported}{14}{0}{2}
\beamer@subsectionintoc {2}{4}{Projects}{21}{0}{2}
\beamer@subsectionintoc {2}{5}{Help}{27}{0}{2}
\beamer@subsectionintoc {2}{6}{Code Live}{31}{0}{2}
\beamer@sectionintoc {3}{Thanks}{32}{0}{3}
