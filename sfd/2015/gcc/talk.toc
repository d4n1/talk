\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{About Me}{3}{0}{1}
\beamer@sectionintoc {2}{GCC}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{About}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Features}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Programming Languages Supported}{12}{0}{2}
\beamer@subsectionintoc {2}{4}{Structure}{21}{0}{2}
\beamer@subsectionintoc {2}{5}{Report Bugs}{25}{0}{2}
\beamer@subsectionintoc {2}{6}{Code Live}{30}{0}{2}
\beamer@sectionintoc {3}{Thanks}{31}{0}{3}
