# Hardwallet

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Wallets

- Hotwallets
- Softwallets
- **Hardwallets**

---

# Vantagens

- Segurança
- Multicoins

---

# Desvantagens

- Furto

---

# Produtos

- Trezor
- Ledger
- Keepkey

---

# Dicas

- Criptografia
- Backup
- Software Live :)

---

# Referências

- [Trezor](https://wiki.trezor.io/Welcome)

---

# d4n1.org

![](assets/avatar.png)
