# Debian Expert Install

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Instalação

- GUI
- CLI
- Expert

---

# Boot

![](assets/install.png)

---

# Advanced Options

![](assets/advanced.png)

---

# Main Menu

![](assets/menu.png)

---

# Partition Disks

![](assets/ecrypt.png)

---

# Main Menu

![](assets/base.png)

---

# Base System

![](assets/targeted.png)

---

# Software Selection

![](assets/standard.png)

---

# Main Menu

![](assets/finish.png)

---

# Pós-Instalação

- Sid
- Shell
- Desktop
- Outros

---

# Vantagens

- Minimalismo
- Tempo
- Controle
- Customização

---

# Referências

- [Debian Documentation](https://www.debian.org/doc/)

---

# d4n1.org

![](assets/avatar.png)
