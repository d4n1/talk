% Debian Packager
% Daniel Pimentel
% Software Freedom Day

# Who I am?

>- Master in Computing
>- Specialist in Software Engineering
>- Graduate in Analysis and Development Systems
>- Technical in Computing
>- Developer
>- Maker
>- Hacktivist
>- Musician
>- Motocycle
>- Skater
>- Geek

# Packages

>- +59k

# Packager

>- dpkg
>- apt
>- apt-get/apt-cache
>- aptitude
>- synaptic

# People

>- Packager
>- Debian Maintainer
>- Debian Developer
>- Sponsor
>- Leader

# Developers

![DDs arround the world](assets/developers.jpg){width=93%}

# Concepts

>- Upstream tarball
>- Source package
>- Binary package

# Docker

```
vim dockerfile
docker build -t debian .
docker run -it debian
```

# Search

```
apt search <upstream>
wnpp-check <upstream>
```

# Upstream

```
wget <upstreal-url>
tar -xvf upstream-1.0.tar.xz
cd upstream-1.0
```

# License

```
licensecheck --deb-machine --merge-licenses -r *
egrep -sriA25 '(copyright|public dom)'
egrep '(@|\[)' <changelog>
```

# Make

```
dh_make -f ../upstream-1.0.tar.xz -c <license>
```

# Packaging

```
cd debian
mv compat <upstream>.* manpage.* menu.* post* pre* \
    README.* watch* *.docs /tmp
vim changelog copyright control rules source/format \
    tests/control watch
```

# Build

```
cd ..
debuild 
```

# Check

```
lintian -i
```

# Scan

```
uscan
```

# Hardening

```
blhc --all --debian <upstream.build>
```

# Install

```
debi
```

# Test

```
mkdir debian/test
vim debian/test/control
autopkgtest . -- null
```

# Patch

```
vim <file>
dpkg-source --commit
quilt push -a
quilt pop -a
```

# Check

```
apt install cowbuilder
cowbuilder create
cowbuilder build <upstream.dsc>
```

# Check

```
debdiff <upstream-1.dsc> <upstream-2.dsc>
```

# Send

```
dput mentors <upstream.changes>
```

# Report (docker)

```
reportbug -b
```

# Tips

>- Gbp

# Mail

>- debian-announce
>- debian-devel-announce
>- debian-infrastructure-announce
>- debian-legal
>- debian-mentors
>- debian-br-eventos

# Site

>- [Tracker](https://tracker.debian.org)
>- [Bug](https://bugs.debian.org)
>- [Packages](https://packages.debian.org)
>- [Lintian](https://lintian.debian.org)
>- [Mentors](https://mentors.debian.net)
>- [Snapshot](https://snapshot.debian.org)

# Help

>- Documentation
>- Translate
>- Packaging
>- Bugs
>- Artwork
>- Mirrors
>- Donations
>- Share :)

# References

>- [Debian Documentation](https://www.debian.org/doc/)
>- [Guia de Empacotamento Debian](
    http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)

# Thanks [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
