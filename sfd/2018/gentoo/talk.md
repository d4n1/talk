% Gentoo: Linux Source Based
% Daniel Pimentel
% Software Freedom Day

# Quem sou eu?

>- Master in Computing
>- Specialist in Software Engineering
>- Graduate in Analysis and Development Systems
>- Technical in Computing
>- Developer
>- Professor
>- Musician
>- Geek

# History

>- Daniel Robbins
>- Enoch (1999)
>- EGGS
>- Gentoo (2000)
>- Gentoo Foundation

# Architectures

>- Alpha
>- AMD64
>- HPPA
>- IA64
>- MIPS
>- PPC
>- PPC64
>- SPARC
>- x86

# Kernel

>- Linux
>- FreeBSD
>- Hurd

# Portage

```
emerge --sync
emerge-webrsync
emerge --search neovim
emerge --ask app-editors/neovim
emerge --unmerge vim
emerge --update --deep --with-bdeps=y --newuse @world
```

# Ebuild

```
mkdir -p /usr/local/portage/app-misc/hello-world 
cd $_
nvim ./hello-world-1.0.ebuild
# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the MIT
```

# Ebuild

```
EAPI=6

DESCRIPTION=""
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

ebuild hello-world-1.0.ebuild manifest clean merge
```

# Make

>- CFLAGS / CXXFLAGS
>- USE

# Packages

>- Stable
>- Masked
>- Hard masked

# Overlays

>- Custom Tree
>- Alternative

# OpenRC

```
rc-update show
rc-update delete dropbox default
install -d /etc/runlevels/$runlevel
```

# OpenRC

```
nvim /etc/conf.d/hello-world
#!/sbin/openrc-run
  
depend() {
#  (Dependency information)
}
  
start() {
#  (Commands necessary to start the service)
}
  
stop() {
#  (Commands necessary to stop the service)
}
```

# Stages

>- Stage1
>- Stage2
>- Stage3

# Profiles

>- Developer
>- Desktop
>- Server

# Logo and masmcots

>- Silver magatama (official)
>- Larry The Cow
>- Znert the Flying Saucer

# Contribute

>- Documentation
>- Ebuilds
>- Bugs
>- Artwork
>- Infrastructure
>- Mirror
>- Donate

# Community

>- IRC channels
>- Discussion forums
>- Mailing lists

# References

>- https://wiki.gentoo.org/wiki/Handbook:Main_Page

# Thanks [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
