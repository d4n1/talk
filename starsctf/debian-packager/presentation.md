# Empacotamento Debian com Docker

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Debian

![](assets/debian.png)

---

# Docker

![](assets/docker.png)

---

# Init

    !zsh
    git clone git@gitlab.com:d4n1/package.git

---

# vim dockerfile

    !docker
    FROM debian:sid

    RUN apt update
    RUN apt install -y autopkgtest blhc devscripts dh-make dput-ng \
        how-can-i-help locales quilt renameutils spell splitpatch \
        git gnupg tree wget neovim

    ADD conf/bashrc /etc/bash.bashrc
    ADD conf/locale.gen /etc/locale.gen
    ADD conf/sources.list /etc/apt/sources.list
    ADD conf/lintianrc /etc/lintianrc
    ADD conf/devscripts.conf /etc/devscripts.conf
    ADD conf/vimrc /root/.vimrc
    ADD conf/quiltrc /root/.quiltrc
    ADD conf/.gnupg /root/.gnupg
    ADD packages /root/packages

    WORKDIR /root/packages

    VOLUME packages /root/packages

---

# vim conf/bashrc

    !zsh
    export DEBFULLNAME="Daniel Pimentel"
    export DEBEMAIL="d4n1@d4n1.org"
    export EDITOR="vim"
    export LANG=C.UTF-8
    export LANGUAGE=C.UTF-8
    export LC_ALL=C.UTF-8
    export QUILT_PATCHES=debian/patches
    alias v=vim

---

# vim conf/locale.gen

    !zsh
    en_US.UTF-8 UTF-8

---

# vim conf/lintianrc

    !zsh
    display-info = yes
    pedantic = yes
    display-experimental = yes
    color = auto

---

# vim conf/devscripts.conf

    !zsh
    DEBSIGN_KEYID=D9F9EADD6F26AC782CFCD8317FECE9042C7BFD45
    DEBUILD_DPKG_BUILDPACKAGE_OPTS="-us -uc -I -i"
    DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"

---

# vim conf/vimrc

    !vim
    syntax on
    set encoding=utf8
    set textwidth=79
    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set autoindent
    set expandtab
    set smarttab
    set laststatus=2
    set ignorecase
    set list
    set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
    hi MatchParen cterm=none ctermbg=gray ctermfg=black

---

# Build

    !zsh
    docker build -t debian .

---

# Run

    !zsh
    docker run -it -v `pwd`/packages:/root/packages debian

---

# Destroy

    !zsh
    docker images -a | grep "debian" | awk '{print $3}' | \
        xargs docker rmi

---

# vim makefile

    !make
    build:
        docker build -t debian .
    run:
        docker run -it -v `pwd`/packages:/root/packages debian
    destroy:
        docker images -a | grep "debian" | awk '{print $3}' | \
            xargs docker rmi --force

---

# Referências

- [Debian Documentation](https://www.debian.org/doc/)
- [Guia de Empacotamento Debian](http://eriberto.pro.br/debian/guia_empacotamento_debian_2.18.pdf)
- [Docker Documentation](https://docs.docker.com/)

---

# d4n1.org

![](assets/avatar.png)
