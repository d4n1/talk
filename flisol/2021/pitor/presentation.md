# Anonymous' Box

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Raspberry Pi


![](assets/rpi.png)


---

# Tor

![](assets/tor.png)

---

# Dark Web

![](assets/dark.jpeg)

---

# Motivação

- Anonimato
- Privacidade
- Open Source
- Fácil :)

--- 

# Requerimentos

- Raspberry Pi (>= 3) 
- MicroSD (Classe 10)

---

# Init

    !bash
    wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-03-25/2021-03-04-raspios-buster-armhf-lite.zip
    sudo dd if=2021-01-11-raspios-buster-armhf-lite.img of=/dev/sdb bs=1MB

---

# Instalação

    !bash
    sudo raspi-config
    sudo apt update
    sudo apt upgrade
    sudo apt install tor

---

# sudo vim /etc/network/interfaces

    !vim
    allow-hotplug eth0
    iface eth0 inet static
    address 192.168.8.42
    netmask 255.255.255.0
    gateway 192.168.8.1

---

# sudo vim /etc/tor/torrc


    !vim
    SocksPort 192.168.8.42:8042
    SocksPolicy accept 192.168.8.0/24
    RunAsDaemon 1
    DataDirectory /var/lib/tor

---

# Serviço

    !bash
    sudo systemctl restart tor@default.service

---

# Referências

- [Raspberry Pi](https://www.raspberrypi.org/software/operating-systems/#raspberry-pi-os-32-bit)
- [Tor](https://2019.www.torproject.org/docs/documentation.html.en)

---

# d4n1.org

![](assets/avatar.png)
