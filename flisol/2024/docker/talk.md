%title: Why Debian Container?
%author: Daniel (d4n1)
%date: 2024-04-30

-> Why Debian Container? <-

-> Daniel (d4n1) <-

----

-> Whoami <-

<br>
- Master in Computing
<br>
- Specialist in Software Engineering
<br>
- Graduate in Analysis and Development Systems
<br>
- Technical in Computing
<br>
- Developer
<br>
- Biker
<br>
- Musician
<br>
- Geek

----

-> Debian <-

<br>
- Free Software
<br>
- Social Contract
<br>
- Stable 
<br>
- Secure
<br>
- Hardware support (+10)
<br>
- Packages (+60k)
<br>
- Community

----

-> Container <-

<br>
- Chroot
<br>
- LXC
<br>
- Cgroups
<br>
- Systemd
<br>
- Docker

----

-> Docker <-

<br>
- Open Source (Docker Inc)
<br>
- Modularity
<br>
- Layers and image version control
<br>
- Rollback
<br>
- Rapid deployment


----

-> DockerHub <-

<br>
- Registry
<br>
- Manage Docker images
<br>
- Debian images (+10k)

---


-> Chroot on Debian <-

<br>
~~~~
apt install debootstrap
mkdir -p ~/debian
debootstrap bullseye ~/debian http://deb.debian.org/debian
chroot ~/debian
~~~~

----

-> LXC on Debian <-

<br>
~~~~
apt install lxc libvirt0 libpam-cgfs bridge-utils uidmap
lxc-create -n debian -t debian -- -r bullseye
lxc-start --name debian
lxc-info --name debian
lxc-stop --name debian
~~~~

----

-> Docker on Debian <-

<br>
~~~~
apt install ca-certificates curl
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update
apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

docker run hello-world
~~~~

----

-> Why Debian Container? <-

<br>
- Base image
<br>
- Stable
<br>
- Minimal
<br>
- Runtime performance
<br>
- Packages
<br>
- Community

----

-> Benchmarking <-

<br>
~~~~
docker build -t <container> .
docker run -p <port>:5000 -t <continer>
wrk -t 4 -c 40 -d 40 http://localhost:<port>
~~~~

---

-> Show me the code :) <-

<br>
- Let's coding!

---

-> Thanks :) <-

[d4n1](https://d4n1.org)
