# Linux Packager

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Package

- Código
- Metadata

---

# Métodos

- Tarball
- Package Managers

---

# APT 

- Nativo
- +51k
- Seguro
- Customizável

---

# Flatpak

- Independente
- Sandbox
- Flathub
- Mais espaço

---

# Snap

- Canonical
- Sandbox
- SnapStore
- Mais lento

---

# AppImage

- Sem instalação
- Sem permissões especiais
- AppImageHub
- Mais rápido

---

# Install (Tarball)

    !bash
        wget <path.tar.gz>
        tar -xzvf <package.tar.gz>
        cd <package>
        ./configure
        make
        make install

---

# Install (APT)

    !bash
        apt update
        apt search <package>
        apt install <package>

---

# Install (Flatpak)

    !bash
        apt install flatpak
        flatpak remotes
        flatpak remote-add --if-not-exists flathub \ 
            https://flathub.org/repo/flathub.flatpakrepo
        flatpak search <package>
        flatpak install <package>

---

# Install (Snap)

    !bash
        apt install snapd
        snap install snap-store
        snap find <package>
        snap install <package>

---

# Install (AppImage)

    !bash
        wget <appImageHub.AppImage>
        chmod u+x <package.AppImage>
        ./<package.AppImage>

---

# Debate

- Melhor?
- Pior?
- Vantagens?
- Futuro?

---

# Referências

- [Debian](https://debian.org/)

---

# d4n1.org

![](assets/avatar.png)
