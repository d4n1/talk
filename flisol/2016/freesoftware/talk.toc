\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Software Livre}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Software Livre}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Manifesto GNU}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Liberdades}{7}{0}{1}
\beamer@subsectionintoc {1}{4}{Licen\IeC {\c c}as}{12}{0}{1}
\beamer@sectionintoc {2}{Software Livre em tempo sombrios}{16}{0}{2}
\beamer@subsectionintoc {2}{1}{Amea\IeC {\c c}as}{16}{0}{2}
\beamer@subsectionintoc {2}{2}{Resist\IeC {\^e}ncia}{22}{0}{2}
\beamer@sectionintoc {3}{Fim}{33}{0}{3}
\beamer@subsectionintoc {3}{1}{Refer\IeC {\^e}ncias}{33}{0}{3}
\beamer@subsectionintoc {3}{2}{Obrigado}{34}{0}{3}
