%{
#include <iostream>
using namespace std;
#define END -1
%}
%%
[0-9]+ return atoi(yytext);
<<EOF>> return END;
%%

int main(int argc, char *argv[]) {
  int total=0, valor=0;
  do {
    valor = yylex();
    if (valor != END) {
      total += valor;
    }        
  } while (valor != END);
  cout << "Total: " << total << endl;
}
