\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{An\IeC {\'a}lise Sint\IeC {\'a}tica}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Analisador Sint\IeC {\'a}tico}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Reconhecimento de Senten\IeC {\c c}as}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Valida\IeC {\c c}\IeC {\~a}o de Senten\IeC {\c c}a}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Reconhecimento de Senten\IeC {\c c}a}{11}{0}{1}
\beamer@subsectionintoc {1}{5}{Valida\IeC {\c c}\IeC {\~a}o Top-Down}{16}{0}{1}
\beamer@subsectionintoc {1}{6}{Valida\IeC {\c c}\IeC {\~a}o Bottom-Up}{24}{0}{1}
\beamer@subsectionintoc {1}{7}{Deriva\IeC {\c c}\IeC {\~a}o Can\IeC {\^o}nica}{32}{0}{1}
\beamer@subsectionintoc {1}{8}{\IeC {\'A}rvore Sint\IeC {\'a}tica}{36}{0}{1}
\beamer@subsectionintoc {1}{9}{Gram\IeC {\'a}tica Amb\IeC {\'\i }guas}{42}{0}{1}
\beamer@subsectionintoc {1}{10}{Analisadores Sint\IeC {\'a}ticos}{47}{0}{1}
\beamer@subsectionintoc {1}{11}{Geradores de Analisadores Sint\IeC {\'a}ticos}{52}{0}{1}
\beamer@subsectionintoc {1}{12}{C\IeC {\'o}digo}{56}{0}{1}
\beamer@sectionintoc {2}{Refer\IeC {\^e}ncias}{59}{0}{2}
\beamer@sectionintoc {3}{Fim}{61}{0}{3}
