\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{An\IeC {\'a}lise Sem\IeC {\^a}ntica}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Analisador Sem\IeC {\^a}ntico}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{An\IeC {\'a}lise do C\IeC {\'o}digo}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Tabela de S\IeC {\'\i }mbolos}{9}{0}{1}
\beamer@subsectionintoc {1}{4}{Decora\IeC {\c c}\IeC {\~o}es de Nomes}{14}{0}{1}
\beamer@subsectionintoc {1}{5}{Informa\IeC {\c c}\IeC {\~o}es de escopo}{19}{0}{1}
\beamer@subsectionintoc {1}{6}{Heur\IeC {\'\i }stica para a An\IeC {\'a}lise Sem\IeC {\^a}ntica}{24}{0}{1}
\beamer@subsectionintoc {1}{7}{Verifica\IeC {\c c}\IeC {\~a}o de Tipos}{28}{0}{1}
\beamer@subsectionintoc {1}{8}{Verifica\IeC {\c c}\IeC {\~a}o de Fluxo de Controle}{33}{0}{1}
\beamer@subsectionintoc {1}{9}{Verifica\IeC {\c c}\IeC {\~a}o de Unidade}{37}{0}{1}
\beamer@sectionintoc {2}{Refer\IeC {\^e}ncias}{42}{0}{2}
\beamer@sectionintoc {3}{Fim}{43}{0}{3}
