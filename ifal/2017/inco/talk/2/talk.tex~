\documentclass{beamer}

\mode<presentation>
\usetheme{Rochester}
\usecolortheme{dove}
\setbeamercovered{dynamic}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{listings}
\usepackage{multicol}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows}

\title[Engenharia de Software]{Introdução a Compiladores}

\author{Daniel Pimentel}

\institute[IFAL] {
  \includegraphics[scale=0.6]{static/img/ifal}
}

\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[allowframebreaks]{Agenda}
  \tableofcontents
\end{frame}

\section{Teoria das Linguagens Formais}

\subsection{Introdução}

\begin{frame}{Introdução \cite{1}}
  \begin{enumerate}
  \item<2-> Teoria das Linguagens Formais
  \item<3-> Sintaxe
  \item<4-> Semântica
  \end{enumerate}
\end{frame}

\subsection{Alfabeto}

\begin{frame}{Alfabeto \cite{2}}
  \begin{enumerate}
  \item<2-> Alfabeto ($\sum$)
  \item<3-> Conjunto finito de símbolos/caracteres
  \item<4-> {a, b, c, d}
  \item<5-> $\varnothing$
  \end{enumerate}
\end{frame}

\subsection{Palavra}

\begin{frame}{Palavra \cite{3}}
  \begin{enumerate}
  \item<2-> Cadeia de caracteres ou sentenças sobre um alfabeto
  \item<3-> $\sum$ = {a, b, c, d} $\to$ {ab, bc, abcd, ...}
  \item<4-> $\varepsilon$ (palavra vazia)
  \item<5-> $\sum^{*}$
  \item<6-> $\sum^{+} \to \sum^{*} - {\varepsilon}$
  \end{enumerate}
\end{frame}

\subsection{Linguagem Formal}

\begin{frame}{Linguagem Formal \cite{4}}
  \begin{enumerate}
  \item<2-> $L \subseteq \sum^{*}$
  \item<3-> $\sum = {a, b} \to {a, bb, aba, abba, bbbb, ...}$
  \end{enumerate}
\end{frame}

\subsection{Linguagem de Programação}

\begin{frame}{Linguagem Formal \cite{5}}
  \begin{enumerate}
  \item<2-> Conjunto de todos os programas (palavras) da linguagem
  \end{enumerate}
\end{frame}

\subsection{Gramática}

\begin{frame}{Gramática \cite{1}}
  \begin{enumerate}
  \item<2-> Conjunto finito de regras que geram palavras
  \item<3-> Palavras geradas por uma gramática define a linguagem
  \item<4-> Gramática de Chomsky (irrestrita ou gramática)
  \end{enumerate}
\end{frame}

\begin{frame}{Gramática de Chomsky}
  \begin{enumerate}
  \item<2-> G = (V, T, P, S)
  \item<3-> V = conjunto finito de símbolos variáveis ou não-terminais
  \item<4-> T = conjunto finito de símbolos terminais disjunto de V
  \item<5-> Regras de produção = $P:(V \cup T)^{+} (V \cup T)^{*}$
  \item<6-> Elemento de V chamado de símbolo inicial 
  \end{enumerate}
\end{frame}

\begin{frame}{Exemplos de Gramática}
  \begin{enumerate}
  \item<2-> Gramática regular (linguagens regulares)
  \item<3-> G = ({S, A, B}, {0, 1}, P, S)
  \item<4-> $P = {S \to 0B, S \to 1A, A \to 0, A \to 0S, B \to 1, B \to
    1S}$
  \item<5-> G = ({S, R}, {0, 1}, P, S)
  \item<6-> $P = {S \to 0B, S \to 1R, R \to 1R, R \to \varepsilon}$
  \end{enumerate}
\end{frame}

\subsection{GCC}

\begin{frame}{GCC}
  \begin{enumerate}
  \item<2-> \textit{GNU Compiler Collection (GCC)}
  \item<3-> Richard Stallman (1987)
  \item<4-> Software Livre (GPL)
  \item<5-> Multiplataforma
  \item<6-> Suporte para várias linguagens
  \end{enumerate}
\end{frame}

\begin{frame}{Funcionamento do GCC}
  \only<2->{
    \begin{figure}
      \includegraphics[scale=0.5]{static/img/gcc}
      \caption{Etapas da compilação no GCC}
    \end{figure}
  }
\end{frame}

\begin{frame}{Fases da compilação do GCC}
  \begin{enumerate}
  \item<2-> Pré-processamento (\#include, \#if, \#define)
  \item<3-> Compilação (assembly)
  \item<4-> Montagem (GNU Assembler $\to$ .o)
  \item<5-> Ligação (GNU Linker $\to$ .0 + libs)
  \end{enumerate}
\end{frame}

\begin{frame}{Estrutura do Compilador GCC}
  \begin{enumerate}
  \item<2-> Front-end (sintaxe)
  \item<3-> Midle-end (otimização)
  \item<4-> Back-end (binário)
  \end{enumerate}
\end{frame}

\subsection{Exemplo}

\begin{lstlisting}[language=c, frame=single, numbers=left]
  #include <stdio.h>

  double test_loop(double d, unsigned n) {
    double x = 1.0;
    unsigned j;
    for (j = 1; j <= n; j++) {
      x *= d;
    }
    return x;
  }

  int main(void) {
    double sum = 0.0;
    unsigned i;
    for (i = 1; i <= 100000000; i++) {
      sum += test_loop(i, i % 5);
    }
    printf("s = %g\n", sum);
    return 0;
  }  
\end{lstlisting}

\subsection{Teste}

\begin{lstlisting}[language=bash, frame=single, numbers=left]
  # test 1
  gcc -Wall -O0 test.c -Im
  time ./a.out

  # test 2
  gcc -Wall -O1 test.c -Im
  time ./a.out

  # test 3
  gcc -Wall -O2 test.c -Im
  time ./a.out

  # test 4
  gcc -Wall -O3 test.c -Im
  time ./a.out

  # test 5
  gcc -Wall -O3 -funroll-loops test.c -Im
  time ./a.out

  # test 6
  gcc -fdump-tree-<generic> text.c
\end{lstlisting}

\subsection{Exercício}

\begin{frame}{Exercício}
  \begin{enumerate}
  \item<2-> Benchmarch entre o GCC e outro compilador
  \end{enumerate}
\end{frame}

\section{Referências}

\begin{frame}[allowframebreaks]{Referências}
  \footnotesize{
    \bibliography{talk}
    \bibliographystyle{apalike}
  }
\end{frame}

\section{Fim}

\begin{frame}
  \begin{figure}
    \includegraphics[scale=0.8]{static/img/logo}
    \caption{d4n1.org}
  \end{figure}  
\end{frame}

\end{document}
