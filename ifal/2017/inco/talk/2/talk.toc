\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Aut\IeC {\^o}matos}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Introdu\IeC {\c c}\IeC {\~a}o}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Caracter\IeC {\'\i }sticas}{8}{0}{1}
\beamer@subsectionintoc {1}{3}{Aut\IeC {\^o}matos Finitos}{11}{0}{1}
\beamer@subsectionintoc {1}{4}{Aut\IeC {\^o}mato Finito Determin\IeC {\'\i }stico}{30}{0}{1}
\beamer@subsectionintoc {1}{5}{Aut\IeC {\^o}mato Finito N\IeC {\~a}o-determin\IeC {\'\i }stico}{34}{0}{1}
\beamer@subsectionintoc {1}{6}{AFN x AFND}{38}{0}{1}
\beamer@subsectionintoc {1}{7}{Exerc\IeC {\'\i }cio}{41}{0}{1}
\beamer@sectionintoc {2}{Refer\IeC {\^e}ncias}{43}{0}{2}
\beamer@sectionintoc {3}{Fim}{44}{0}{3}
