\documentclass{beamer}

\mode<presentation>
\usetheme{Rochester}
\usecolortheme{dove}
\setbeamercolor{structure}{fg=red}
\setbeamercolor{frametitle}{fg=red}
\setbeamercovered{dynamic}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{multicol}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows}

\title[Desenvolvimento de Software]{Algoritmo e Lógica de Programação}

\author{Daniel Pimentel}

\institute[IFAL]{\includegraphics[scale=0.6]{static/img/ifal}}

\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Exercicio}
  \begin{itemize}
  \item<2-> Efetuar a leitura de quatro números inteiros e apresentar
    os que são divisíveis por 5
  \item<3-> Efetuar a leitura de quatro números inteiros e apresentar
    os que são divisíveis por 2 e 3
  \item<4-> Efetuar a leitura de cinco números inteiros e informar o
    maior e o menor valor
  \item<5-> Faça um algoritmo que lê a idade de 5 pessoas e escreva
    quantas delas possuem idade igual ou superior a 18 anos
  \item<6-> Faça um algoritmo que lê 4 valores e apresente a média dos pares
  \item<7-> Faça um algoritmo que lê o preço de uma caixa de fósforos
    e o preço de um isqueiro. Sabendo-se que um isqueiro pode ser
    acionado 1300 vezes e que uma caixa de fósforos contém 40 palitos,
    escreva qual dos recursos é mais barato
  \end{itemize}
\end{frame}

\begin{frame}{Exercicio}
  \begin{itemize}
  \item<2-> Faça um algoritmo que lê a peso e a altura de 4
    pessoas. Apresente a média de peso entre as pessoas com mais de
    1,80 de altura. Apresente ainda, a média de altura das pessoas com
    mais de 100 Kg
  \item<3-> Faça um algoritmo que lê um ano e apresente na tela se
    este é bissexto ou não
  \item<4-> Faça um algoritmo que lê as dimensões A, B e C de uma
    sala retangular. Imagine que o piso será trocado e as paredes
    internas e externas pintadas. Leia também o tipo de piso a ser
    utilizado, bem como os tipos de tinta a serem utilizadas
    distintamente dentro e fora da sala. Baseado na tabela de preços
    abaixo, calcule o custo de material desta reforma. Sabe-se que um
    litro de tinta cobre $3m^2$ de parede.  Tabela de Pisos
    (tipo/preço por metro quadrado): 1/24.00, 2/31.00, 3/55.00. Tabela
    de Tintas (tipo/preço por litro): 1/22.00, 2, 47.00    
  \end{itemize}
\end{frame}

\begin{frame}{Exercicio}
  \begin{itemize}
  \item<2-> Faça um programa que leia 4 números inteiros e apresente:
    média dos ímpares, maior número par, diferença do maior menos o
    menor número
  \item<3-> Faça um programa que leia o nome e a idade de 4 pessoas e
    apresente: maior idade, nome da pessoa mais nova, média das idades
  \item<4-> Faça um programa que leia o nome, salário e a quantidade
    de dependentes dos 4 funcionários de uma pequena
    empresa. Apresente: nome do(s) funcionário(s) que ganha(m) até R\$
    500,00 e possuem mais de 3 dependentes, nome e quantidade de
    dependentes do funcionário que ganha o maior salário
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{Referências}
  \footnotesize{
    \bibliography{talk}
    \bibliographystyle{apalike}
  }
\end{frame}

\begin{frame}
  \begin{figure}
    \includegraphics[scale=2]{static/img/logo}
    \caption{d4n1.org}
  \end{figure}  
\end{frame}

\end{document}
