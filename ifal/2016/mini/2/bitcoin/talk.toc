\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Miner}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Bitcoin miner}{3}{0}{1}
\beamer@sectionintoc {2}{Hardware para minera\IeC {\c c}\IeC {\~a}o}{23}{0}{2}
\beamer@subsectionintoc {2}{1}{Hardware}{23}{0}{2}
\beamer@subsectionintoc {2}{2}{Pool de minera\IeC {\c c}\IeC {\~a}o}{78}{0}{2}
\beamer@sectionintoc {3}{Anonimato}{92}{0}{3}
\beamer@subsectionintoc {3}{1}{Anonimato b\IeC {\'a}sico}{92}{0}{3}
\beamer@subsectionintoc {3}{2}{Como deanonimar o Bitcoin}{116}{0}{3}
\beamer@subsectionintoc {3}{3}{Mixing}{125}{0}{3}
\beamer@sectionintoc {4}{Bitcoin}{131}{0}{4}
\beamer@subsectionintoc {4}{1}{Bitcoin core software}{131}{0}{4}
\beamer@subsectionintoc {4}{2}{Ra\IeC {\'\i }zes do Bitcoin}{145}{0}{4}
\beamer@sectionintoc {5}{Fim}{155}{0}{5}
