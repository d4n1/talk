---
title: Informática Básica e Aplicada
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Professor
>- Pesquisador
>- Músico
>- Geek

# Arduino

>- Hardware
>- Software
>- Comunidade

# Plataforma

>- Atmel
>- Wiring
>- Open Source

# Arduino

![Placa Arduino Uno R3](assets/arduino.png)

# Desenvolvimento

>- Sketch
>- setup()
>- loop()

# Ambiente

![Ambiente de d esenvolvimento do Arduino](assets/environment.png)

# Softwares

>- Arduino SDK
>- Fritzing

# Exercício

>- Criar um modelo no Fritzing

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
