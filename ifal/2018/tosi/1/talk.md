---
title: Tópicos Especiais de Sistema de Informação
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Professor
>- Pesquisador
>- Músico
>- Geek

# Princípios de Prevenção e Protecção

>- Relação custo/benefício
>- Concentração
>- Proteção em profundidade
>- Consistência do plano
>- Redundância

# Fortaleza da Informação

>- Política
>- Integridade do monitor
>- Secretismo

# Sobrevivência da Informação

>- Envolvimento
>- Exposição
>- Emergência
>- Diversidade
>- Contexto

# Modelo de Maturidade

>- Definição de políticas e normas de segurança
>- Definição da arquitectura e dos processos da segurança
>- Implementação dos processos de suporte à inspecção, protecção, detecção e reacção
>- Realização de acções de sensibilização e de formação em segurança
>- Realização periódica de auditorias e testes à segurança
>- Implementação de processos de resposta reflexa
>- Validação do modelo de protecção e da sua implementação

# Atores da Segurança

>- Administração da Empresa
>- Utilizadores
>- Informática
>- Clientes
>- Parceiros
>- Pessoal Temporário

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
