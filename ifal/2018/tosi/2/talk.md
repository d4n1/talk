---
title: Tópicos Especiais em Sistema de Informação
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Gestão dos Riscos

>- Identificação dos riscos
>- Análise de risco
>- Identificação de controles
>- Selecção de controles

# Identificação dos Riscos

>- SWOT (strengths, weaknesses, opportunities and threats)
>- Contexto
>- Alvo
>- Bens

# Ameaças

>- Árvore de ameaças

# Vulnerabilidades

>- Árvore de vulnerabilidades

# Bens

>- Escalão

# Análises de Risco e de Impacto

>- Análise de Risco Quantitativa
>- Análise de Risco Qualitativa

# Análise de Risco Quantitativa

>- ALE = Valor × R
>- ALE: perda monetária média experada num ano
>- Valor: valor acumulado dos danos provocados pela concretização da ameaça
>- R: probabilidade de concretização da ameaça na empresa no período de um ano

# Análise de Risco Quantitativa

>- R = V × P
>- V: número que representa a vulnerabilidade da empresa à ameaça
>- P: probabilidade correspondente ao número médio esperado de vezes que a ameaça se irá concretizar por ano

# Exemplo

>- considerando uma empresa com um datacenter avaliado em 300.000, situado por baixo de uma cantina, num edifício equipado com sistemas de incêndio por água, qual é o ALE do risco de perda do equipamento do datacenter devido a um incêndio?
>- Considerando que em média poderá ocorrer um incêndio grave a cada cem anos, que o tipo de mecanismo de incêndio provocará a destruição dos equipamentos em caso de ativação e que a presença da cantina aumenta em 50% o risco de incêndio:
>- P = 1/100 = 0,01
>- V = 1,0 (normal) + 0,5 (da cantina) + 0,2 (do sistema de incêndio)
>- R = 0,01 × 2,0 = 0,02
>- ALE = 300.000 × 0,02 = 6,000

# Exemplo

>- Segundo esta metodologia, o risco de perda do equipamento em causa corresponde a uma exposição da ordem dos seis mil por ano, podendo-se então justificar a introdução de controles com um custo anual abaixo deste valor

# Análise de Risco Qualitativa

>- Constituição da equipe
>- Realização de sessões de classificação das ameaças
>- Realização de sessões de classificação dos impactos
>- Cálculo dos riscos
>- Risco = Probabilidade + Impacto

# Exemplo

![Ficha de classificação das ameaças](assets/1.png)

# Exercício

>- Realizar a análise de risco quantitativa
>- Realizar a análise de risco qualitativa

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
