---
title: Tópicos Especiais em Sistema de Informação
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Análise de Impacto no Negócio

>- Top-down
>- Visão mais estratégica até à operacional

# Arquitetura de Segurança

![Arquitetura de Segurança dos Sistemas de Informação](assets/1.png){width=60%}

# Exemplo

>- Diagrama de ligação de dispositivos
>- Funções da arquitetura
>- Zonas de rede

# Abordagens ao Controle de Riscos

>- Q1 - Aceitação
>- Q2 - Transferência (por exemplo, seguro)
>- Q3 - Redução (da frequência e impacto do risco)
>- Q4 - Evasão (redução da concretização do risco)

# Abordagens ao Controle de Riscos

![Regras baseadas no mapa de risco](assets/2.png)

# Maturidade

>- Definição de políticas e normas de segurança
>- Definição da arquitetura e dos processos da segurança
>- Implementação dos processos de suporte à inspecção, protecção, detecção e reação
>- Realização de ações de sensibilização e de formação em segurança
>- Realização periódica de auditorias e testes à segurança
>- Implementação de processos de resposta reflexa
>- Validação do modelo de proteção e da sua implementação

# Análise Custo/Benefício

>- Retorno sobre o investimento (ou Return on Investment - ROI)
>- Bug do ano 2000

# Análise Custo/Benefício

>- R_b = (V - V_c ) × P
>- ALE_b = Valor × R_b

# Análise Custo/Benefício

>- R_b : redução da probabilidade de concretização da ameaça na Empresa no período de um ano
>- V: número que representa a vulnerabilidade da Empresa à ameaça
>- V_c : número que representa a redução da vulnerabilidade da Empresa à ameaça
>- P: probabilidade correspondente ao número médio esperado de vezes que a ameaça irá se concretizar
>- ALE_b : redução na perda monetária média experável num ano

# Exemplo

>- O benefício da introdução de um sistema de supressão de fogo por CO2 que permita a extinção de um fogo no datacenter sem danificar os equipamentos poderia resultar em um novo valor para V
>- V = 1,0 (normal) + 0,5 (da cantina) - 0,2 (do sistema de extinção)
>- R_b = 0,01 × 1,3 = 0,013
>- ALE_b = 300.000 × 0,013 = 3,900
>- Deste modo, uma redução da exposição à perda de seis mil para três mil e novecentos por ano

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
