---
title: Fundamentos de Programação para Internet
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# HTML5 Example

```
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
</head>

<body>
Content of the document......
</body>

</html> 
```

# New HTML5 API


>- HTML Geolocation
>- HTML Drag and Drop
>- HTML Local Storage
>- HTML Application Cache
>- HTML Web Workers
>- HTML SSE 

# HTML History

>- 1989 Tim Berners-Lee invented www
>- 1991 Tim Berners-Lee invented HTML
>- 1995 HTML Working Group defined HTML 2.0
>- 2000 W3C Recommendation: XHTML 1.0
>- 2014 W3C Recommendation: HTML5

# New Form Elements

```
<datalist>
<output>
```

# HTML5 - New Attribute Syntax

```
<input type="text" value="John" disabled>
<input type="text" value=John>
<input type="text" value="John Doe">
```

# HTML5 Graphics

```
<canvas>
<svg>
```

# New Media Elements

```
<audio>
<embed>
<source>
<track>
<video>
```

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
