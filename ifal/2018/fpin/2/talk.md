---
title: Fundamentos de Programação para Internet
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# HTML5 Semantic Elements

>- Claramente descritos
>- Suporte para os modernos browsers

# New Semantic Elements in HTML5

```
<article>
<aside>
<details>
<figcaption>
<figure>
<footer>
<header>
<main>
<mark>
<nav>
<section>
<summary>
<time>
```

# HTML5 section Element

```
<section>
  <h1>WWW</h1>
  <p>The World Wide Web (WWW) is....</p>
</section> 
```

# HTML5 article Element

```
<article>
  <h1>What Does WWW Do?</h1>
  <p>WWW's mission is ...</p>
</article> 
```

# HTML5 header Element

```
<article>
  <header>
    <h1>What Does WWW Do?</h1>
    <p>WWW's mission:</p>
  </header>
  <p>WWF's mission is ...</p>
</article> 
```

# HTML5 footer Element

```
<footer>
  <p>Posted by: Daniel Pimentel</p>
  <p>Contact information: <a href="mailto:d4n1@d4n1.org">
    d4n1@d4n1.org</a>.
  </p>
</footer> 
```

# HTML5 nav Element

```
<nav>
  <a href="/html/">HTML</a> |
  <a href="/css/">CSS</a> |
  <a href="/js/">JavaScript</a> |
  <a href="/jquery/">jQuery</a>
</nav>
```

# HTML5 aside Element

```
<p>W3C.</p>

<aside>
  <h4>W3C</h4>
  <p>W3C is ...</p>
</aside>
```

# HTML5 figure and figcaption Elements

```
<figure>
  <img src="ifal.jpg" alt="IFAL">
  <figcaption>Fig1. - IFAL.</figcaption>
</figure> 
```

# Exercício

>- Desenvolver o portofólio pessoal em HTML5 semântico

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
