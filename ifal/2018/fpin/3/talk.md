---
title: Fundamentos de Programação para Internet
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# What is CSS?

>- CSS (Cascading Style Sheets)
>- Descreve como os elementos HTML serão apresentados

# CSS Syntax

```
p {
    color: red;
    text-align: center;
}
```

# The id Selector

```
#para1 {
    text-align: center;
    color: red;
}
```

# The class Selector

```
.center {
	text-align: center;
    color: red;
}

p.center {
    text-align: center;
    color: red;
}

<p class="center large">Refers to two classes.</p> 
```

# Grouping Selectors

```
h1, h2, p {
    text-align: center;
    color: red;
}
```

# CSS Comments

```
p {
    color: red;
    /* This is a single-line comment */
    text-align: center;
}

/* This is
a multi-line
comment */
```

# Insert CSS

>- External style sheet
>- Internal style sheet
>- Inline style

# External Style Sheet

```
<head>
<link rel="stylesheet" type="text/css" href="style.css">
</head> 
```

# Internal Style Sheet

```
<head>
<style>
body {
    background-color: linen;
}

h1 {
    color: maroon;
    margin-left: 40px;
}
</style>
</head> 
```

# Inline Styles

```
<h1 style="color:blue;margin-left:30px;">Style</h1>
```

# Exercício

>- Adicionar CSS no portofólio pessoal feito em HTML5

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
