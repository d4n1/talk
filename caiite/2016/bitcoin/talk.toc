\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Introdu\IeC {\c c}\IeC {\~a}o}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Bitcoin}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Ecosistema}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Transa\IeC {\c c}\IeC {\~a}o}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Assinatura de Grupo}{10}{0}{1}
\beamer@sectionintoc {2}{Problema}{12}{0}{2}
\beamer@sectionintoc {3}{Metodologia}{17}{0}{3}
\beamer@sectionintoc {4}{Experimento}{23}{0}{4}
\beamer@sectionintoc {5}{An\IeC {\'a}lise}{29}{0}{5}
\beamer@subsectionintoc {5}{1}{Anonimato}{29}{0}{5}
\beamer@subsectionintoc {5}{2}{Desempenho}{31}{0}{5}
\beamer@sectionintoc {6}{Resultado}{33}{0}{6}
\beamer@subsectionintoc {6}{1}{Anonimato}{33}{0}{6}
\beamer@subsectionintoc {6}{2}{Desempenho}{36}{0}{6}
\beamer@sectionintoc {7}{Conclus\IeC {\~a}o}{39}{0}{7}
\beamer@sectionintoc {8}{Refer\IeC {\^e}ncias}{40}{0}{8}
\beamer@sectionintoc {9}{Fim}{42}{0}{9}
