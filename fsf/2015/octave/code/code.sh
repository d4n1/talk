# Numbers
n = 30

# String
s = "FSF30"

# Arithmetic operators
15 + 15
42 - 12
5 * 6
60 / 2
2 ^ 5

# Logical operators
1 === 2
1 ~= 2
1 && 0
1 || 0
xor(1, 0)

# Output
fsf = 30
gnu = fsf;
gnu

# Display
disp(fsf);
disp(sprint('30 years %0.2f', fsf))
format long
format short

# Matrix
gnu = [1 9; 8 3]
fsf = [10; 20; 30]
size(fsf)
gnu = 1:30
ones(2,4)
2 * ones(2, 4)
zeros(1, 4) 
rand(2, 4)
randn(2, 4)
eye(4)

# Histogram
g = -6 + sqrt(10) * (randn(1, 4))
g = rand(1, 100)
hist(g)
close
hist(g, 50)
(hist (rand(1, 100), 25, 'facecolor', 'r', 'edgecolor', 'b');

# Diferencial equations
f = linspace(0, 50, 200)';
d = [1; 2];
g = lsode("f", d, f)

# Plot
plot(g, f)
print -dpdf fsf.pdf

# Help
help eye
help help

# Loading 
load file.csv 

# Variables
who
whos
clear variable_name
clear

# Export
save file g
save file g --ascii