---
title: Git
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Professor
>- Pesquisador
>- Músico
>- Geek

# Introdução

>- Criado por Linus Torvalds
>- Open source

# Fundamentos

>- Controle de versão distribuída
>- Trabalho colaborativo

# Arquitetura

![Sistemas de Controle de Versão Distribuídos](assets/arch.jpg){width='40%'}

# Estágios

![Os três estágios](assets/stage.png){width='80%'}

# Fluxo

![Estado dos arquivos](assets/flux.png){width='60%'}

# Instalação

```
sudo apt install git
```

# Manual

```
git help
git help -a
git help <command>
```

# Identidade

```
git config --global user.name "Daniel Pimentel"
git config --global user.email d4n1@d4n1.org
```

# Editor

```
git config --global core.editor nvim
```

# Criando um repositório

```
git init
```

# Criando um arquivo dentro do diretório

```
echo "Frodo Baggins" > rings
```

# Verificando estado dos arquivos

```
git status
```

# Adicionando arquivos à staging

```
git add rings
```

# Adicionando arquivos ao repositório

```
git commit -m 'Primeiro personagem do Lotr'
```

# Editando arquivos

```
echo "Samwise Gamgee" >> rings
git status
git commit -a -m 'Segundo personagem adicionado do Lotr'
```

# Histórico de commits

```
git log
git log --oneline
```

# Diferenças de commits

```
git diff xxxxxxx yyyyyyy
```

# Rotulando commits

```
git tag -a v1.0 -m 'Sociedade do Anel'
git show v1.0
```

# Branch

![Git branching](assets/branch.png){width='80%'}

# Gerenciamento de branches

```
git branch
git branch fellowship
git branch -v
```

# Navegando pelos commits

```
git checkout fellowship
```

# Modificando o arquivo no novo branch

```
echo "Gandalf" >> rings
git commit -a -m 'Adicionado novo personagem ao Lotr'
```

# Enviando as mudanças para o repositório

```
git checkout master
echo "Legolas" >> rings
echo "Aragorn" >> rings
git commit -a -m 'Adiionado: Legolas e Aragorn'
```

# Mesclando branches

```
git merge fellowship
```

# Resolvendo conflitos

>- master (HEAD)
>- fellowship

# Finalizando o merge

```
git commit -a
```

# Verificando o histórico dos branches

```
git checkout master
git log --oneline
git checkout fellowship
git log --oneline
```

# Clonando um repositório

```
git clone <repositorio>
```

# Repositório remoto

```
git remote add <nome> <url>
git remote -v
```

# Atualizando o repositório local

```
git remote add gitlab <url>
git fetch gitlab
git checkout master
git merge gitlab/master
```

# Atualizando o repositório local

```
git pull gitlab master
```

# Atualizando o repositório remoto

```
git push gitlab master
```

# Git-flow

![Branching workflow](assets/git-flow.png){width='40%'}

# Documentação

>- git-scm.com/book

# Obrigado [^1]

![d4n1.org](assets/avatar.jpg){width='35%'}

[^1]: https://gitlab.com/d4n1/talk
