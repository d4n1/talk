---
title: Python
author: Daniel Pimentel
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Classe

>- Tipo composto
>- Instância ou objetos

# Classe

```python
class Cachorros:
  pass

Cachorros
dir(Cachorros)
Cachorros
id(Cachorros()) == id(Cachorros())
Dogs = Cachorros
Dogs
Dogs.__name__
```

# Atributos

```python
class Cachorros:
  cobertura='pelos'
  alimento='carne'
  patas=4
  habitat='domestico'
  nome='Rex'

class Galinhas:
  cobertura='penas'
  alimento='graos'
  patas=2
  habitat='domestico'
  bico='pequeno'
```

# Atributos

```python
dir(Cachorros)
dir(Galinhas)
Snoopy=Cachorros()
Lala=Galinhas()
Snoopy.alimento
Lala.alimento
Lala.bico
Snoopy.bico
Cachorros.nome
```

# Métodos

```python
class Circulos:
  raio = 25.4
  def calcula_area(self):
    self.area = 3.14*(self.raio**2)
  def calcula_volume(self, altura):
    self.volume = 3.14*(self.raio**2)*altura
```

# Métodos

```python
C1=Circulos()
C1.raio
C1.area
dir(C1)
C1.calcula_area()
C1.area
dir(C1)
C1.calcula_area() == Circulos.calcula_area(C1)
C1 = Circulos()
C1.calcula_volume()
C1.calcula_volume(42)
C1.volume
```

# \__init__

```python
class Cachorros:
  cobertura='pelos'
  alimento='carne'
  patas=4
  habitat='domestico'
  def __init__(self, nome):
    self.nome= nome

d1 = Cachorros('Dog1')
d1.nome
```

# \__init__

```python
from random import random
class Sprites:
  def __init__(self, nome,
               tamanho = 'grande',
               cor = 'amarelo',
               arestas = 5):
    self.nome = nome
    self.tamanho = tamanho
    self.cor = cor
    self.arestas = arestas
  def update_position(self):
    self.position = random()*10,random()*10
    print('%s está agora em %s.', self.nome,
          self.position)
```

# \__init__

```python
s1 = Sprites('Star1', 'pequeno', 'vermelho')
s1.nome, s1.tamanho, s1.cor, s1.arestas
s2 = Sprites('Star2', arestas=6, cor='azul')
s2.nome, s2.tamanho, s2.cor, s2.arestas
s1.update_position(), s2.update_position()
s1.position
s2.position
```

# \__doc__ e \__module__

```python
class Fausto:
  """Fausto é um romance de Goethe
  que Beethoven transformou em Ópera."""
  def review(self):
    """
    Este método responde com a avaliação dos críticos
    """
    print('Um romance excepcional')

print(Fausto.__doc__)
print(Fausto().__doc__)
print(Fausto().review.__doc__)
help(Fausto)
```

# \__doc__ e \__module__

```python
class So_Acredito_Vendo:
  pass

sav = So_Acredito_Vendo()
sav.__module__
from math import sin
sin.__module__
```

# Herança

>- Mãe
>- Filha

# Herança

```python
class Pai:
  nome = 'Byron'
  sobrenome = 'Lovelace'
  residencia = 'Londres'

class Filha(Pai):
  nome = 'Ada'
  olhos = 'castanhos'

class Neta(Filha):
  nome = 'Mary'
```

# Herança

```python
Pai.nome, Filha.nome, Neta.nome
Pai.residencia, Filha.residencia, Neta.residencia
Pai.olhos
Filha.olhos, Neta.olhos
issubclass(Neta,Pai)
Neta.__bases__
```

# Exercício

>- Criar o sistema acadêmico orientado a objetos

# Obrigado [^1]

![d4n1.org](assets/avatar.jpg){width='44%'}

[^1]: https://github.com/d4n1/talk/
