\beamer@endinputifotherversion {3.36pt}
\select@language {brazilian}
\beamer@sectionintoc {1}{Criptografia}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Fun\IeC {\c c}\IeC {\~o}es de hash criptogr\IeC {\'a}ficos}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Ponteiro de hash e estruturas de dados}{12}{0}{1}
\beamer@subsectionintoc {1}{3}{Assinaturas digitais}{20}{0}{1}
\beamer@subsectionintoc {1}{4}{Chaves p\IeC {\'u}blicas como identidades}{30}{0}{1}
\beamer@sectionintoc {2}{Decentraliza\IeC {\c c}\IeC {\~a}o}{45}{0}{2}
\beamer@subsectionintoc {2}{1}{Decentraliza\IeC {\c c}\IeC {\~a}o de arquivos Bitcoin}{45}{0}{2}
\beamer@subsectionintoc {2}{2}{Concenso distribu\IeC {\'\i }do}{49}{0}{2}
\beamer@subsectionintoc {2}{3}{Consensos sem identidade: o block chain}{69}{0}{2}
\beamer@sectionintoc {3}{Proof of Work}{92}{0}{3}
\beamer@subsectionintoc {3}{1}{Incentivos e Proof of Work}{92}{0}{3}
\beamer@sectionintoc {4}{Mecanismos}{127}{0}{4}
\beamer@subsectionintoc {4}{1}{Transa\IeC {\c c}\IeC {\~o}es Bitcoin}{127}{0}{4}
\beamer@subsectionintoc {4}{2}{Scripts Bitcoin}{142}{0}{4}
\beamer@subsectionintoc {4}{3}{Blocos Bitcoin}{163}{0}{4}
\beamer@subsectionintoc {4}{4}{Rede Bitcoin}{173}{0}{4}
\beamer@subsectionintoc {4}{5}{Limita\IeC {\c c}\IeC {\~o}es e melhorias}{198}{0}{4}
\beamer@sectionintoc {5}{Armazenamento}{213}{0}{5}
\beamer@subsectionintoc {5}{1}{Armazenamento local}{213}{0}{5}
\beamer@subsectionintoc {5}{2}{Armazenamento quente ou frio}{228}{0}{5}
\beamer@subsectionintoc {5}{3}{Wallet online e interc\IeC {\^a}mbios}{236}{0}{5}
\beamer@subsectionintoc {5}{4}{Servi\IeC {\c c}os de pagamentos}{250}{0}{5}
\beamer@subsectionintoc {5}{5}{Atual mercado de interc\IeC {\^a}mbio}{266}{0}{5}
\beamer@sectionintoc {6}{Fim}{279}{0}{6}
