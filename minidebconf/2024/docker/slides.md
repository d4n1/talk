---
title: Why Debian Container?
author: Daniel Pimentel
theme: black-contrast
revealOptions:
  transition: 'fade'
---

# Why Debian Container?

---

## Daniel Pimentel

- Master in Computing (UFAL)
- Free Software since ~2000 (Guix, Debian ...)
- Speaker since ~2010 (DebConf, FISL ...)
- Developer since ~2000 (BackEnd, DevOps ...)
- Biker (ride to live :)
- Musician (\m/)
- Geek :)

---

## Debian

- Free Software
- Social Contract
- Stable 
- Secure
- Hardware support (+10)
- Packages (+60k)
- Community

---

## Virtualization x Container

![Virtualization x Container](images/virtualization.png)

---

## Containers

- Chroot
- LXC
- Docker

---

## Chroot

```
apt install debootstrap
mkdir -p ~/debian
debootstrap bullseye ~/debian http://deb.debian.org/debian
chroot ~/debian
```

---

## LXC

```
apt install lxc libvirt0 libpam-cgfs bridge-utils uidmap
lxc-create -n debian -t debian -- -r bullseye
lxc-start --name debian
lxc-info --name debian
lxc-stop --name debian
```

---

## Docker

- Open Source (Docker Inc)
- Modularity
- Layers
- Image version control
- Rollback
- Rapid deployment

---

## Docker Arch

![Docker Architecture](images/docker.png)

---

## DockerHub

- Registry
- Debian images (+10k)

---

## Docker

```
apt install ca-certificates curl
curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update
apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

docker run hello-world
```

---

## Show me the code

```
docker build -t <container> .
docker run -p <port>:5000 -t <continer>
wrk -t <x> -c <y> -d <z> http://localhost:<port> --latency
```

---

## Benchmark (t4 c8000 d30)

<img src='images/benchmark1.jpg' height="250px">

---

## Benchmark (t4 c80000 d120)

<img src='images/benchmark2.jpg' height="250px">

---

## Benchmark (t4 c800000 d120)

<img src='images/benchmark3.jpg' height="250px">

---

## Benchmark (t4 c800000 d240)

<img src='images/benchmark4.jpg' height="250px">

---

## Why Debian Container?

- Base image
- Stable
- Minimal
- Performance
- Compatibility
- Packages
- Community

---

## Documentation

- [Debian](https://debian.org)
- [LXC](https://linuxcontainers.org/)
- [Docker](https://docker.com)

---

# d4n1.org

![d4n1](images/avatar.png)
