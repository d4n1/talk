# Python Packager

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# História

- Setuptools
- Eggs -> Wheels
- EasyInstall -> Pip

---

# Init

    !zsh
    mkdir oxepy
    cd oxepy
    python -m venv .venv
    source .venv/bin/activate
    touch setup.py
    mkdir oxepy
    touch oxepy/__init__.py
    touch oxepy/test.py
    mkdir test

---

# Requisitos

    !zsh
    pip install wheel setuptools

---

# vim oxepy/__init__.py

    !python
    def oxepy_init():
        '''Function init.'''
        print('Oxe Init :)')

---

# vim oxepy/test.py

    !python
    def oxepy_test():
        '''Function test.'''
        print('Oxe test :)')

---

# vim setup.py

    !python
    from setuptools import setup

    setup(
        name='oxepy',
        version='1.0',
        packages=['oxepy'],
    )

---

# vim setup.cfg

    !python
    [metadata]
    license_files =
        license.txt
        3rdparty/*.txt

---

# Build

    !zsh
    python setup.py bdist_wheel

---

# Install

    !zsh
    pip install dist/oxepy-1.0-py3-none-any.whl
    pip freeze > requirements.pip

---

# vim test/main.py

    !python
    from oxepy.__init__ import *
    from oxepy.test import *

    oxepy_init()
    oxepy_test()

---

# Run

    !zsh
    python test/main.py

---

# Converter

    !zsh
    wheel convert *.egg

---

# Publicar

    !zsh
    pip install twine
    twine upload -r testpypi dist/*

---

# Misc

- 339/360 Wheels
- [pythonwheels.com](https://pythonwheels.com/)

---

# Referências

- [Wheel](https://wheel.readthedocs.io/en/stable/index.html)

---

# d4n1.org

![](assets/avatar.png)
