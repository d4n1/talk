% Debian Packager
% Daniel Pimentel
% Campus Party

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Maker
>- Músico
>- Geek

# Início

![Carta de Ian Murdock em 93](assets/ian.jpg){width=44%}

# Nome

>- **Deb**ra Lynn
>- **Ian** Murdock

# Logo

![A logo **redemoinho** representa a magic smoke](assets/logo.png){width=44%}

# Orgiem

>- Software Livre
>- Contrato Social
>- Manifesto Debian

# Software Livre

>- A liberdade de executar o programa
>- A liberdade de estudar o programa
>- A liberdade de redistribuir cópias do programa
>- A liberdade de modificar o programa e redistribuir

# Contrato Social

>- O Debian permanecerá 100% livre
>- Nós iremos retribuir à comunidade software livre
>- Nós não esconderemos problemas
>- Nossas prioridades são nossos usuários e o software livre
>- Programas que não atendem nossos padrões de software livre

# Manifesto Debian

>- Redistribuição livre
>- Código fonte
>- Trabalhos derivados
>- Integridade do código fonte do autor
>- Não à discriminação contra pessoas ou grupos
>- Não à discriminação contra fins de utilização
>- Distribuição de licença
>- A Licença não pode ser específica para o Debian
>- A Licença não deve contaminar outros softwares

# Versões

>- Unstable
>- Testing
>- Stable

# Releases

>- 1.1 Buzz
>- 2 Hamm
>- 2.1 Slink
>- 2.2 Potato
>- 3 Woody
>- 3.1 Sarge
>- 4 Etch
>- 5 Lenny
>- 6 Squeeze
>- 7 Wheezy
>- 8 Jessie
>- 9 Stretch
>- *10 Buster*

# Kernel

>- Linux
>- KfreeBSD
>- Hurd

# Hardware

>- 64-bit PC (amd64)
>- 32-bit PC (i386)
>- EABI ARM (armel)
>- Hard Float ABI ARM (armhf)
>- MIPS (little endian)
>- MIPS (big endian)
>- IBM System z
>- 64-bit ARM (AArch64)
>- POWER Processors
>- 64-bit MIPS (little endian)

# Pacotes

>- dpkg (Debian Package)
>- apt (Advanced Packaging Tool)
>- apt-get e apt-cache, aptitude, synaptic
>- +51k

# Familia

![Árvore da família Debian](assets/tree.jpg){width=30%}

# Pessoas

>- Empacotador
>- Mantenedor (Debian Maintainer)
>- Desenvolvedor (Debian Developer)
>- Sponsor (DD upload)
>- Líder (DD leader)

# Líderes

>- Ian Murdock (1993-1996)
>- Bruce Perens (1996-1998)
>- Ian Jackson (1998-1999)
>- Wichert Akkerman (1999-2001)
>- Ben Collins (2001-2002)
>- Bdale Garbee (2002-2003)
>- Martin Michlmayr (2003-2005)
>- Branden Robinson (2005-2006)
>- Anthony Towns (2006-2007)
>- Sam Hocevar (2007-2008)
>- Steve Mcintyre (2008-2010)
>- Stefano Zacchiroli (2010-2013)
>- Lucas Nussbaum (2013-2015)
>- Neil McGovem (2015-2016)
>- Mehdi Dogguy (2016-2017)
>- Chris Lamb (atual)

# Desenvolvedores

![Desenvolvedores Debian](assets/developers.jpg){width=93%}

# Ciclo de vida

>- Upstream
>- FTP-Master
>- Unstable
>- Testing
>- Stable

# Workflow [^1]

![Mapa geral](assets/workflow.png){width=75%}

[^1]: [Fonte](http://people.debian.org/~madduck/talks/
etch-delay_skycon_2007.02.18/package-cycle.png)

# Controle

>- [FTP­Master](http://ftp-master.debian.org)
>- [Quality Assurance](http://qa.debian.org)
>- [Package Track System (PTS)](http://packages.qa.debian.org)
>- [Busca por pacotes](http://packages.debian.org)
>- [Bug track System (BTS)](http://bugs.debian.org)
>- [WNPP](http://www.debian.org/devel/wnpp)
>- [Processamento de bugs](http://www.debian.org/Bugs/Developer)
>- [Dinstall](http://people.debian.org/~joerg/dinstall.html)
>- [Listas de pacotes](http://packages.debian.org/<release)

# Pre-empacotamento

>- O pacote existe?
>- Tem Sponsor?
>- Verificou os bugs (flawfinder, rats, pscan, splint ...)?

# Intent To Package (ITP)

```
apt install reportbug
echo "export DEBFULLNAME=”Daniel Pimentel” >> ~./zshrc
echo "export DEBEMAIL=”d4n1@d4n1.org” >> ~/.zshrc
reportbug ­-b
```

# Formato

>- ```ar tv <package.deb>```
>- Ficheiro: debian-binary
>- Meta-dados: contro.tar.gz
>- Dados: data.tar.gz

# Feramentas

```
apt install build-essential dpkg-dev devscripts 
```

# Empacotando

```
apt source dash
cd dash-0.5.10.2
debuild -us -uc
ls debian ../
```

# Revisando

```
cd debian
dch -i
debuild
```

# Nova versão

```
apt source <package>
cd <package_folder>
uupdate -u ../<package-version.tar.gz>
```

# Dicas

>- Use o programa empacotado
>- Teste o código
>- Converse com o upstream
>- Leia o Debian Policy
>- Leia o Guia do Novo Mantenedor
>- Leia o Developer's References
>- Use patches
>- Analise o pacote
>- Teste o pacote em desenvolvimento
>- Teste o pacote em produção
>- Contribua :)

# Contribuição

>- Documentação
>- Empacotamento
>- Desenvolvimento
>- Bugs
>- Artwork
>- Infraestrutura
>- Mirror
>- Doações

# Comunidade

>- IRC
>- Forums
>- Emails
>- Redes sociais

# Referências

>- [Debian Documentation](https://www.debian.org/doc/)

# Obrigado [^0]

![Progamming for coffee](assets/avatar.png){width=33%}

[^0]: [d4n1.org](http://d4n1.org)
