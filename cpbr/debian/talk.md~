---
title: Tópicos Sistema de Informação
author: Daniel Pimentel
institute: IFAL
output:
  beamer_presentation:
    incremental: true
    highlight: monochrome
---

# Quem sou eu?

>- Mestre em Informática
>- Especialista em Engenharia de Software
>- Graduado em Análise e Desenvolvimento de Sistemas
>- Técnico em Informática
>- Desenvolvedor
>- Professor
>- Pesquisador
>- Músico
>- Geek

# Geek?

![Anatomia de um programador Geek](assets/geek.jpg){width=48%}

# Guix?

![GNU Guix](assets/guix.png){width=60%}

# Características

>- GNU Guile
>- GPL
>- Transactional
>- Environment
>- Unprivileged

# Pacotes

>- +7k

# Arquitetura

>- x86_64
>- i686
>- armhf
>- aarch64

# Shell

```
guix package -s emacs
guix package -i emacs
guix package -r vi
guix package -u
guix package -l
```

# Shell

```
guix pull
guix build emacs
guix edit gcc@4.9
guix download
guix hash
guix import gnu hello
guix refresh
guix lint
guix size coreutils
guix graph coreutils
```

# Shell

```
guix environment guile
guix publish
guix challenge --substitute-urls="https://hydra.gnu.org \
    https://guix.example.org"
guix copy --from=host libreoffice gimp
guix container exec 9001 \
    /run/current-system/profile/bin/bash --login
guix weather --substitute-urls=https://guix.example.org
guix system docker-image
```

# Package

```
(use-modules (guix)
             (guix build-system gnu)
             (guix licenses))
(package
  (name "package")
  (version "1.0")
  (source (origin
            (method url-fetch)
     (uri (string-append "url-"  version
                                ".tar.gz"))
            (sha256
             (base32
              "asdf7789798ad577adf788"))))
  (build-system gnu-build-system)
  (synopsis "Package ...")
```
# Package

```
  (description "Package ...")
  (home-page "http://www.package.org")
  (license gpl3+))
```

# Compile

```
git clone git://git.savannah.gnu.org/guix.git
cd guix
guix package -i guile libgcrypt make \
    guile-json gnutls sqlite bzip2 gcc \
    gcc-toolchain make autoconf automake \
    gettext texinfo graphviz help2man pkg-config
./bootstrap
./configure --localstatedir=/var
make
make check
```

# Compile

```
guix download http://url-package.tar.gz
emacs gnu/packages/package.scm
./pre-inst-env guix build package
./pre-inst-env guix package -i package
```

# Shepherd

```
herd start apache
herd status
herd detailed-status
herd status apache
herd stop networking
herd enable apache
herd reload-modules apache
```

# GuixSD

```
loadkeys abnt2
ifconfig interface up
nano wpa_supplicant.conf
wpa_supplicant -c wpa_supplicant.conf -i interface -B
dhclient -v interface
parted /dev/sda
mkfs.ext4 -L my-root /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2
mount LABEL=my-root /mnt
```

# GuixSD

```
herd start cow-store /mnt
mkdir /mnt/etc
cp /etc/configuration/desktop.scm /mnt/etc/config.scm
nano /mnt/etc/config.scm
guix system init /mnt/etc/config.scm /mnt
```

# Software

>- GNU Shepherd
>- Guix
>- GuixSD
>- *Emacs*

# Ajuda

>- help-guix@gnu.org
>- guix-devel@gnu.org
>- bug-guix@gnu.org
>- info-gnu@gnu.org

# Referências

>- https://www.gnu.org/software/guix/
>- https://www.gnu.org/software/guix/manual/en/

# Obrigado [^1]

![d4n1.org](assets/avatar.png)

[^1]: d4n1.org
