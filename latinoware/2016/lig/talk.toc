\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Me}{4}{0}{1}
\beamer@sectionintoc {2}{Introduction}{13}{0}{2}
\beamer@subsectionintoc {2}{1}{Context}{13}{0}{2}
\beamer@subsectionintoc {2}{2}{Problem}{15}{0}{2}
\beamer@sectionintoc {3}{Lightom}{23}{0}{3}
\beamer@subsectionintoc {3}{1}{Lightom}{23}{0}{3}
\beamer@subsectionintoc {3}{2}{Libre Hardware}{25}{0}{3}
\beamer@subsectionintoc {3}{3}{Layout}{27}{0}{3}
\beamer@subsectionintoc {3}{4}{Network}{29}{0}{3}
\beamer@subsectionintoc {3}{5}{Free Software}{31}{0}{3}
\beamer@subsectionintoc {3}{6}{Resume}{33}{0}{3}
\beamer@subsectionintoc {3}{7}{New Problem}{40}{0}{3}
\beamer@sectionintoc {4}{Lig}{45}{0}{4}
\beamer@subsectionintoc {4}{1}{IoT}{45}{0}{4}
\beamer@subsectionintoc {4}{2}{GNU Guile}{51}{0}{4}
\beamer@subsectionintoc {4}{3}{Lig}{57}{0}{4}
\beamer@sectionintoc {5}{References}{59}{0}{5}
\beamer@sectionintoc {6}{End}{60}{0}{6}
