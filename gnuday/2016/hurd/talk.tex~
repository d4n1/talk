\documentclass{beamer}

\mode<presentation>
\usetheme{Rochester}
\usecolortheme{dove}
\setbeamercovered{dynamic}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{multicol}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows}

\title[Guile]{\includegraphics[scale=0.4]{static/img/guile.png}}
\author{Daniel Pimentel}
\institute{Software Freedom Day}
\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}[allowframebreaks]{Roadmap}
  \tableofcontents
\end{frame}

\section{Me}

\begin{frame}{Me}
  \begin{itemize}
  \item<2-> Master in Computing
  \item<3-> Post-graduate in Software Engineer
  \item<4-> Graduate in Analysis and Development System
  \item<5-> Free Software Developer
  \item<6-> SysAdmin
  \item<7-> Professor
  \item<8-> Research
  \item<9-> Freelancer
  \item<10-> Hacktivist
  \end{itemize}
\end{frame}

\section{GNU Guile}

\subsection{Guile?}

\begin{frame}
  \frametitle{Guile?}
  \only<2>{
    \begin{figure}
      \includegraphics[scale=0.5]{static/img/guile-flash-kick}
      \caption{Guile flash kick \cite{1}}
    \end{figure}}
\end{frame}

\subsection{Guile}

\begin{frame}
  \frametitle{Guile}
  \only<2>{
    \begin{figure}
      \includegraphics[scale=0.5]{static/img/guile}
      \caption{GNU Ubiquitous Intelligent Language for Extensions \cite{2}}
    \end{figure}}
\end{frame}

\subsection{Features}

\begin{frame}{Features}
  \begin{itemize}    
  \item<2-> Developed by GNU (GNU's Not Unix)
  \item<3-> Distributed by FSF (Free Software Foundation)
  \item<4-> GPL (GNU Public License)
  \item<5-> Scheme's interpreter and compiler 
  \item<6-> Official extension language for the GNU operating system
  \end{itemize}
\end{frame}

\subsection{Programming Languages Supported}

\begin{frame}{Programming Languages Supported}
  \begin{itemize}
  \item<2-> Scheme
  \item<3-> ELisp (Emacs Lisp)
  \item<4-> C
  \item<5-> C++ 
  \item<6-> ECMAScript (Javascript)
  \item<7-> Lua, Python and Ruby (soon)
  \end{itemize}
\end{frame}

\subsection{Projects}

\begin{frame}{Projects}
  \begin{itemize}
  \item<2-> \textbf{Core:} Guile-Lib, Guile-Lint, Guile-Reader, Guile syntax parse
  \item<3-> \textbf{GUI:} Guile-GNOME, Guile-GTK, Guile-Ncurses,
    Guile-SDL, libRUIN
  \item<4-> \textbf{Networking:} GnuTLS, Guile-Avahi, Guile-RPC, SCSS,
    Ragnarok
  \item<5-> \textbf{Tools:} AutoGen, Libchop
  \item<6-> \textbf{Application:} GNU Guix, GuixSD, GnuCash, gEDA,
    GDB, Beast, LilyPod, Skribilo, Snd, TeXmacs, XChat-Guile, Maggic,
    Lightom
  \end{itemize}
\end{frame}

\subsection{Help}

\begin{frame}{Help}
  \begin{itemize}
  \item<2-> Report bugs to bug-guile@gnu.org
  \item<3-> Send a patch to guile-devel@gnu.org
  \item<4-> Contributing to Guile development (guile-devel@gnu.org)
  \end{itemize}
\end{frame}

\subsection{Coding}

\begin{frame}{Coding}
  \lstinputlisting{static/code/code.scm}
\end{frame}

\section{References}

\begin{frame}[allowframebreaks]{References}
  \footnotesize{\bibliography{talk} \bibliographystyle{apalike}}
\end{frame}

\section{End}

\begin{frame}
  \begin{figure}
    \includegraphics[scale=0.4]{static/img/dani}
  \end{figure}  
  \Huge{\centerline{d4n1.org}}
\end{frame}

\end{document} 
