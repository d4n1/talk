# Docker

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Introdução

- Virtualização
- Container

---

# Docker


![](assets/docker.png)

---

# Conceitos

- Imagens
- Containers
- Register

---

# Instalação

    !zsh
    apt install docker docker.io

---

# Instalação CE

    !zsh
    sudo apt-get install ca-certificates curl gnupg lsb-release

    sudo curl -fsSL https://download.docker.com/linux/debian/gpg | \
      sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    sudo apt update
    sudo apt install docker-ce docker-ce-cli containerd.io

---

# Teste

    !zsh
    docker --version
    docker version
    docker info

---

# Hello World

    !zsh
    docker run hello-world
    docker image ls
    docker container ls --all

---

# vim dockerfile

    !vim
    FROM python:2.7-slim
    WORKDIR /app
    COPY . /app
    RUN pip install -r requirements.pip
    EXPOSE 80
    ENV NAME World
    CMD ["python", "app.py"]

---

# vim requirements.pip

    !vim
    flask
    redis

---

# vim app.py

    !python
    from flask import Flask
    from redis import Redis, RedisError
    import os
    import socket

    redis = Redis(
      host="redis",
      db=0,
      socket_connect_timeout=2,
      socket_timeout=2
    )

    app = Flask(__name__)

    @app.route("/")
    def hello():
      try:
        visits = redis.incr("counter")
      except RedisError:
        visits = "<i>cannot connect to Redis, counter disabled</i>"
      
      html = "<h3>Hello {name}!</h3>" \
             "<b>Hostname:</b> {hostname}<br/>" \
             "<b>Visits:</b> {visits}"

      return html.format(
        name=os.getenv("NAME", "world"),
        hostname=socket.gethostname(),
        visits=visits
      )

    if __name__ == "__main__":
      app.run(host='0.0.0.0', port=80)

---

# Build

    !zsh
    docker build -t app .

---

# Run

    !zsh
    docker run -d -p 5000:80 app

---

# Share

    !zsh
    docker login
    docker tag app d4n1/app:latest
    docker push d4n1/app:latest

---

# Pull

    !zsh
    docker run -p 5000:80 d4n1/app:latest

---

# Desafio

- Crie 2 containers
- Defina seu Dockerfile
- Faça upload para o Dockerhub

---

# Referências

- [Docker](https://www.docker.com/)

---

# d4n1.org

![](assets/avatar.png)
