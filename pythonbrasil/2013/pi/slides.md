# **Raspberry Pi e Python:** Inovação tecnológica de baixo custo

---

# Daniel Pimentel (d4n1)

- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Projetos e Administração de Redes
- Técnico em Projetos e Análise de Sistemas
- LPIC (Linux Professional Institute Certified) 
- NCLA (Novel Certified Linux Administrator)
- Desenvolvedor Open Source (Python, C/C++, HTML, CSS, ...)
- SysAdmin Open Source (GNU/Linux, GNU/Hurd, BSD)
- Entusiasta por Open Design (Gimp, Inkscape, Blender, ...)
- Entusiasta por Open Hardware (Arduino, Raspberry Pi, ...)
- Hacktivista

---

# O que é o **Raspberry Pi**?

![](img/raspberry_pi.png)

---

# Pequeno, mas **poderoso**!

![](img/pequeno.png)

---

# Por menos de **US$ 35**!

![](img/barato.png)

---

# Educacional

![](img/educacional.png)

---

# Hackeável!

![](img/hackeavel.png)

---

# Modelo A ou B?

![](img/models.png)

---

# Mini PC Pythônico

![](img/inside.jpg)

---

# Outros mini PC's bombados

![](img/asteroides.png)

---

# Quais S.O's ?

![](img/so.png)

---

# Raspbian (Debian)

![](img/raspbian.png)

---

# Baterrias inclusas?

![](img/battery.png)

---

# LXDE

![](img/lxde.png)

---

# Python 2.x e 3.x

![](img/python.png)

---

# Pygame

![](img/pygame.png)

---

# Scratch

![](img/scratch.png)

---

# Pi + Arduino

![](img/arduino.jpg)

---

# Makers

![](img/professor_pardal.png)

---

# Acessórios

![](img/tool.png)

---

# Kits

![](img/kit.png)

---

# Breadboard

![](img/breadboard.jpg)

---

# GPIO

![](img/gpio.jpg)

---

# Como programar o Pi?

![](img/maker.jpg)

---

# Em ShellScript (on.sh)

Criando o arquivo on.sh
    
    !bash
    touch on.sh

Editando o arquivo on.sh
    
    !bash
    #!/bin/bash
    echo Exporting pin $1
    echo $1 > /sys/class/gpio/export
    echo Setting direction to out
    echo out > /sys/class/gpio/gpio$1/direction
    echo Setting pin high
    echo 1 > /sys/class/gpio/gpio$1/value

Executando o script
    
    !bash
    chmod +x on.sh
    $ sudo ./on.sh 25

---

# Em ShellScript (off.sh)

Criando o arquivo off.sh

    !bash
    touch off.sh

Editando o arquivo off.sh

    !bash
    #!/bin/bash
    echo Setting pin low
    echo 0 > /sys/class/gpio/gpio$1/value
    echo Unexporting pin $1
    echo $1 > /sys/class/gpio/unexport

Executando

    !bash
    chmod +x off.sh
    sudo ./off.sh 25

---

# Em Python (blink.py)

Criando o arquivo blink.py
    
    !bash
    touch blink.py 

Editando o arquivo blink.py

    !python
    import RPi.GPIO as GPIO
    import time

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(25, GPIO.OUT)
    GPIO.setup(8, GPIO.OUT)

    while True:
        GPIO.output(25, GPIO.HIGH)
        GPIO.output(8, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(25, GPIO.LOW)
        GPIO.output(8, GPIO.LOW)
        time.sleep(1)

Executando

    !bash
    sudo python blink.py

---

# Em Python (button.py)

Criando o arquivo button.py

    !bash
    touch button.py 

Editando o arquivo button.py

    !python
    import RPi.GPIO as GPIO
    import time

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(24, GPIO.IN)
    GPIO.setup(25, GPIO.OUT)
    GPIO.setup(8, GPIO.OUT)

    while True:
        input_value = GPIO.input(24)

        if (input_value == True):
            GPIO.output(25, GPIO.HIGH)
            GPIO.output(8, GPIO.HIGH)
        elif (input_value == False):
            GPIO.output(25, GPIO.LOW)
            GPIO.output(8, GPIO.LOW)

Executando

    !bash
    sudo python button.py

---

# Pi na Web com **Flask**

![](img/pi_web.jpg)

---

# Flask Project

    !bash
    mkdir weblamp; cd weblamp
    touch app.py
    mkdir templates; mkdir static
    touch templates/main.html
    touch templates/layout.html
    touch templates/404.html
    touch README.md
    touch requeriments.txt
    touch fabfile.py
    touch .hgignore
    hg init
    hg add

---

# app.py

    !python
    # encoding: utf-8
    import RPi.GPIO as GPIO
    from flask import Flask, render_template, request

    app = Flask(__name__)

    GPIO.setmode(GPIO.BCM)

    pins = {
        25: {'name':u'lâmpada da sala', 'state':GPIO.LOW},
        8: {'name':u'lâmpada do quarto', 'state':GPIO.LOW}
        }

    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)


    @app.route('/')
    def main():
        for pin in pins:
            pins[pin]['state'] = GPIO.input(pin)
        
        template_data = {'pins': pins}
        return render_template('main.html', **template_data)


    @app.route('/<int:change_pin>/<action>')
    def action(change_pin, action):
        device_name = pins[change_pin]['name']

        if action == 'on':
            GPIO.output(change_pin, GPIO.HIGH)
            message = u'Dispositivo ' + device_name + ' ligado'

        if action == 'off':
            GPIO.output(change_pin, GPIO.LOW)
            message = u'Dispositivo ' + device_name + ' desligado'

        if action == 'toggle':
            GPIO.output(change_pin, not GPIO.input(change_pin))
            message = 'Dispositivo ' + device_name + ' alternado'

        for pin in pins:
            pins[pin]['state'] = GPIO.input(pin)

        template_data = {'message':message, 'pins':pins}
        return render_template('main.html', **template_data)


    @app.errorhandler(404):
    def page_not_found(error):
        return render_template('404.html'), 404


    if __name__ == '__main__':
        app.run(host='0.0.0.0', port=80, debug=True)

---

# layout.html

    !html
    <!DOCTYPE html>
    <!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
    <!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />

        <!-- Set the viewport width to device width for mobile -->
        <meta name="viewport" content="width=device-width" />

        <title>Weblamp | {% block title %}Principal{% endblock %}</title>

        <link rel="stylesheet" href="{{ url_for('static', filename='css/normalize.css') }}">
        <link rel="stylesheet" href="{{ url_for('static', filename='css/foundation.css') }}">

        <script src="{{ url_for('static', filename='js/vendor/custom.modernizr.js') }}"></script>
    </head>

    <body>
        <div class="row">
            <div class="large-12 columns">
                <div class="nav-bar right">
                    <ul class="button-group">
                        <li><a href="{{ url_for('main') }}" class="button">Principal</a></li>
		        <li><a href="#" class="button">Login</a></li>
                    </ul>
                </div>
                <h1>Weblamp <small>Rasbperry Pi Flask.</small></h1>
                <hr />
            </div>
        </div>

        <div class="row">
            {% block content %}
            {% endblock %}        
        </div>

        <footer class="row">
            <div class="large-12 columns">
                <hr />
                <div class="row">
                    <div class="large-6 columns">
                         <p>&copy; Oryg.in</p>
                    </div>
        
                    <div class="large-6 columns">
                       <ul class="inline-list right">
                           <li><a href="{{ url_for('main') }}">Principal</a></li>
                           <li><a href="#">Login</a></li>
                       </ul>
                    </div>
                </div>
            </div>
        </footer>

        <script src="{{ url_for('static', filename='js/foundation.min.js') }}"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
    </html>

---

# 404.hml

    !html
    {% extends 'layout.html' %}

    {% block content %}
         404
    {% endblock %}

---

# main.html

    !html
    {% extends 'layout.html' %}

    {% block content %}
        <div class="large-12 columns" role="content">
            <article>
                <h3><a href="#">Gerenciar Lâmpadas</a></h3>        

	            {% for pin in pins %}
	                <h4>
                            A {{ pins[pin].name }}
	                
                            {% if pins[pin].state == true %}
	                        está acesa. <a href="/{{ pin }}/off" class="small button alert">Desligar</a>
	                    {% else %}
	                        esta apagada. <a href="/{{ pin }}/on" class="small button">Ligar</a>
	                    {% endif %}
	                </h4>
	            {% endfor %}

	            {% if message %}
	                <h2>{{ message }}</h2>
	            {% endif %}
      	    </article>
        </div>
    {% endblock %}

---

# Sua imaginação é o limite!

![](img/coyote.png)

---

# Micro Arcade

![](img/micro_arcade.jpg)

---

# Owncloud (Dropbox clone)

![](img/owncloud.jpg)

---

# RTor

![](img/rtor.jpg)

---

# BeetBox

![](img/beet_box.jpg)

---

# Netlight

![](img/netlight.jpg)

---

# Pwn (PenTest)

![](img/pwn.png)

---

# Xbmc

![](img/xbmc.jpg)

---

# Rtank

![](img/rtank.png)

---

# Quadcopter

![](img/quadcopter.jpg)

---

# RCluster

![](img/cluster.jpg)

---

# Raspberry Pi, mini PC Pythônico

![](img/raspy.jpg)

---

# That's all folks!