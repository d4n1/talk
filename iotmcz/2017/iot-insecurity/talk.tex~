\documentclass{beamer}
\mode<presentation>
\usetheme{Luebeck}
\usecolortheme{beaver}
\setbeamercolor{structure}{fg=red}
\usepackage{textpos}
\setbeamercovered{dynamic}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[utf8]{inputenc}
\usepackage[brazilian]{babel}
\usepackage[T1]{fontenc}
\usepackage{listings}
\usepackage{multicol}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{shapes, arrows}

\title{Shell Script}
\author{Daniel Pimentel}
\institute{Mostra IFAL}

\date{\today}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Eu}
  \begin{enumerate}
  \item<2-> Mestre em Informática
  \item<3-> Especialista em Engenharia de Software
  \item<4-> Graduado em Análise e Desenvolvimento de Sistemas
  \item<5-> Técnico em Informática
  \item<6-> Desenvolvedor
  \item<7-> Professor
  \item<8-> Pesquisador
  \item<9-> Músico
  \item<10> Geek
  \end{enumerate}
\end{frame}

\begin{frame}{Shell}
  \only<2>{
    \begin{figure}
      \includegraphics[scale=.25]{static/image/bash}
      \caption{Bash \it{Bourne-Again SHell} \cite{1}}
    \end{figure}
  }
\end{frame}

\begin{frame}{Script}
  \only<2>{
    ``Linguagem de script interpretada'' \cite{1}
  }
\end{frame}

\begin{frame}{Shell script}
  \only<2->{
    ``Script em shell'' \cite{1}
  }
\end{frame}

\begin{frame}{Características}
  \begin{itemize}
  \item<2-> Decisão
  \item<3-> Repetição
  \item<4-> Funções e argumentos
  \item<5-> Variáveis
  \item<6-> Escopo
  \end{itemize}
\end{frame}

\begin{frame}{História}
  \begin{itemize}
  \item<2-> Chet Ramey (1989)
  \item<3-> Multi plataforma (POSIX)
  \item<4-> GNU \it{GNU's Not Unix}
  \item<5-> GPL \it{GNU Public License}
  \end{itemize}
\end{frame}

\begin{frame}{Ambiente}
  \begin{itemize}
  \item<2-> .bash\_profile
  \item<3-> .bashrc
  \item<4-> /etc/profile
  \end{itemize}
\end{frame}

\begin{frame}{Código}
  \only<2->{
    \lstinputlisting[language=bash, frame=single, numbers=left,
      linerange={1-2}]{static/code/code}
  }

  \only<3->{
    \lstinputlisting[language=bash, frame=single, numbers=left,
      linerange={3-5}]{static/code/code}
  }
\end{frame}

\begin{frame}[allowframebreaks]{Referências}
  \footnotesize{
    \bibliography{talk}
    \bibliographystyle{apalike}
  }
\end{frame}

\begin{frame}
  \begin{figure}
    \includegraphics[scale=2]{static/image/logo}
    \caption{\url{d4n1.org}}
  \end{figure}  
\end{frame}

\end{document}
