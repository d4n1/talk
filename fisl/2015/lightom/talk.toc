\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{About}{3}{0}{1}
\beamer@sectionintoc {2}{Context}{4}{0}{2}
\beamer@sectionintoc {3}{Problems}{5}{0}{3}
\beamer@sectionintoc {4}{Lightom}{17}{0}{4}
\beamer@subsectionintoc {4}{1}{Light On Management}{17}{0}{4}
\beamer@subsectionintoc {4}{2}{Arduino}{18}{0}{4}
\beamer@subsectionintoc {4}{3}{Layout}{19}{0}{4}
\beamer@subsectionintoc {4}{4}{Application}{20}{0}{4}
\beamer@subsectionintoc {4}{5}{Network}{21}{0}{4}
\beamer@subsectionintoc {4}{6}{Resume}{22}{0}{4}
\beamer@subsectionintoc {4}{7}{Prototype}{29}{0}{4}
\beamer@sectionintoc {5}{Future Works}{32}{0}{5}
\beamer@sectionintoc {6}{References}{36}{0}{6}
