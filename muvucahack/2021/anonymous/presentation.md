# Anonimato em Tempos Sombrios

---

# Daniel Pimentel

- Mestre em Informática
- Especialista em Engenharia de Software
- Graduado em Análise e Desenvolvimento de Sistemas
- Técnico em Informática
- Desenvolvedor
- Guitarrista
- Motociclista
- Geek

---

# Anônimo

- Sem nome

---

# Anonimato

- Não identificação

---

# Privacidade

- Vida privada

---

# Existe Anonimato?


![](assets/anonymous.png)

---

# Internet

![](assets/internet.png)

---

# Espionagem Global

![](assets/spy.jpg)

---

# NSA

![](assets/nsa.png)

---

# GCHQ

![](assets/gchq.png)

---

# Técnicas

- MYSTIC (Interceptação de áudio)
- NUCLEON (Análise de dados)
- DISHFIRE (Interceptação de mensagens SMS)
- PREFER (Identificação por categoria)
- PINWALE (Bancos de dados para cruzamento de informação)

---

# Redes

- Tor
- Freenet
- GnuNet
- Matrix

---

# Buscadores

- DuckDuckGo
- SearX
- YaCy

---

# Redes Sociais

- Reddit
- Mastodon
- GnuSocial
- Chan

---

# Email

- GnuPG

---

# Finanças

- Criptomoedas

---

# Open Hardware

- Arduino
- BeagleBone
- Novena
- Eoma68
- Libreboot
- Risc-V

---

#  Sistemas Operacionais

- Distribuições Gnu/Linux
- BSDs

---

# Smartphones

- AOSP
- LineageOS
- Replicant
- Tizen
- Plasma
- Ubuntu Touch
- PostmarketOS
- Mobian
- SailfishOS

---

# Mensageiros

- IRC
- Signal
- Element
- Dino
- Quicksy
- Briar
- Jami
- Tox

---

# Existe Anonimato?

- Offline :)
- Open Hardware!
- Open Source!
- Free Software!

---

# Referências

- [Debian Documentation](https://www.debian.org/doc/)

---

# d4n1.org

![](assets/avatar.png)
